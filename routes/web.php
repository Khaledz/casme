<?php
// Auth::logout();
Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('app/public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
Auth::routes();
// verify not working!
Route::get('/verify/', ['uses' => '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm', 'as' => 'verification.verify']);
Route::get('/register-social-media/{provider}', '\App\Http\Controllers\Auth\RegisterController@redirectToProvider');
Route::get('/register-social-media/{provider}/callback', '\App\Http\Controllers\Auth\RegisterController@handleProviderCallback');

Route::get('/register/', ['uses' => '\App\Http\Controllers\Auth\RegisterController@showRegistrationForm']);
Route::get('/register/{role}', ['uses' => '\App\Http\Controllers\Auth\RegisterController@index']);
Route::post('/register/member', ['uses' => '\App\Http\Controllers\Auth\RegisterController@register_member']);
Route::post('/register/beneficiary', ['uses' => '\App\Http\Controllers\Auth\RegisterController@register_beneficiary']);
Route::post('/register/uploadHands', ['uses' => '\App\Http\Controllers\Auth\RegisterController@upload_hands_pictures']);
Route::get('/', ['uses' => 'HomeController@index', 'as' => 'app.home.index']);
Route::get('/contact-us', ['uses' => 'HomeController@contact', 'as' => 'app.home.contact']);
Route::post('/contact-us', ['uses' => 'HomeController@post_contact', 'as' => 'app.home.contact']);
Route::get('/terms', ['uses' => 'HomeController@terms', 'as' => 'app.home.term']);
Route::get('/privacy', ['uses' => 'HomeController@privacy', 'as' => 'app.home.privacy']);
Route::get('/about', ['uses' => 'HomeController@about', 'as' => 'app.home.about']);
Route::get('lang/{locale}', 'LocalizationController@index');

Route::post('/categories-requirements', ['uses' => 'CategoryController@getRequirements', 'as' => 'app.category.requirements']);
 Route::group(['middleware' => 're'], function () {
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/successful-payment', ['uses' => 'PaymentController@successful', 'as' => 'app.payment.successful']);    
    // Route::get('/failed-payment', ['uses' => 'PaymentController@failed', 'as' => 'app.payment.failed']);    
    
    Route::get('/user/settings', ['uses' => 'UserController@settings', 'as' => 'app.user.setting']);
    Route::post('/user/settings', ['uses' => 'UserController@post_setting', 'as' => 'app.user.setting']);

    Route::get('/set-category', ['uses' => 'UserController@set_category_get', 'as' => 'app.user.set_category']);
    Route::post('/set-category', ['uses' => 'UserController@set_category_post', 'as' => 'app.user.set_category']);
    Route::get('/user/dashboard', ['uses' => 'UserController@dashboard', 'as' => 'app.user.dashboard']);
    Route::get('/user/orders', ['uses' => 'UserController@orders', 'as' => 'app.user.order']);
    Route::get('/user/payments', ['uses' => 'UserController@payments', 'as' => 'app.user.payment']);
    Route::get('/user/bank', ['uses' => 'UserController@bank', 'as' => 'app.user.bank']);
    Route::get('/user/portofolios', ['uses' => 'UserController@portofolios', 'as' => 'app.user.portofolio']);
    Route::resource('/user', 'UserController', ['as' => 'app']);
    Route::resource('/order', 'OrderController', ['as' => 'app']);
    Route::get('/category/filter/', ['uses' => 'CategoryController@filter', 'as' => 'app.category.filter']);
    Route::resource('/category', 'CategoryController', ['as' => 'app']);
    Route::resource('/portofolio', 'PortofolioController', ['as' => 'app']);
    Route::resource('/payment', 'PaymentController', ['as' => 'app']);
});

