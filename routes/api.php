<?php

Route::post('login', 'Api\Auth\AuthController@login');
Route::post('register/member', 'Api\Auth\AuthController@register_member');
Route::post('register/beneficiary', 'Api\Auth\AuthController@register_beneficiary');
Route::post('countries', 'Api\ListAllController@countries');
Route::post('categories', 'Api\ListAllController@categories');
Route::post('genders', 'Api\ListAllController@genders');
Route::post('rates', 'Api\ListAllController@rates');
Route::post('accents', 'Api\ListAllController@accents');
Route::post('social_medias', 'Api\ListAllController@social_medias');
Route::post('price_types', 'Api\ListAllController@price_types');
Route::post('hair_colors', 'Api\ListAllController@hair_colors');
Route::post('eye_colors', 'Api\ListAllController@eye_colors');
Route::post('menu', 'Api\ListAllController@menu');
Route::post('skin_colors', 'Api\ListAllController@skin_colors');
Route::post('payment_methods', 'Api\ListAllController@payment_methods');
Route::post('payment_statuses', 'Api\ListAllController@payment_statuses');
Route::post('app_setting', 'Api\ListAllController@app_setting');
Route::post('roles', 'Api\ListAllController@roles');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('logout', 'Api\Auth\AuthController@logout');
    Route::get('getAuthUser', 'Api\Auth\AuthController@user');
    Route::resource('category', 'Api\CategoryController');
    Route::post('category/requirements', 'Api\CategoryController@getRequirements');
    Route::post('category/filter', 'Api\CategoryController@filter');
    Route::get('/getHomeCategories', 'Api\HomeController@index');

    Route::resource('order', 'Api\OrderController');
    Route::resource('portofolio', 'Api\PortofolioController');
    Route::resource('payment', 'Api\PaymentController');
    Route::get('/check-payment-status', 'Api\PaymentController@successful');

    Route::get('/dashboard', 'Api\UserController@dashboard');
    Route::post('/setting', 'Api\UserController@post_setting');
    Route::get('/user/payments', 'Api\UserController@payments');
    Route::get('/user/orders', 'Api\UserController@orders');
    Route::get('/user/portofolios', 'Api\UserController@portofolios');
    Route::post('/user/set-category', 'Api\UserController@set_category_post');
    Route::resource('user', 'Api\UserController');
    Route::get('/user', 'Api\UserController@show');
    Route::get('/app-setting', 'Api\AppSettingController@index');
});