<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_accents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('user_skin_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('user_eye_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('user_hair_colors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('user_genders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('category_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::create('category_requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('validations');
            $table->timestamps();
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->integer('category_type_id')->unsigned()->nullable();
            $table->boolean('in_home')->default(0);
            $table->timestamps();

            $table->foreign('category_type_id')->references('id')->on('category_types')->onDelete('cascade');
        });

        Schema::create('categories_requirements', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('category_requirement_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('category_requirement_id')->references('id')->on('category_requirements')->onDelete('cascade');
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('avatar')->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->integer('height')->unsigned()->nullable();
            $table->integer('weight')->unsigned()->nullable();
            $table->integer('age')->unsigned()->nullable();
            $table->string('video')->nullable();
            $table->string('embed_video')->nullable();
            $table->longText('about')->nullable();
            $table->boolean('status')->default(0);
            $table->boolean('is_travel')->default(0)->nullable();
            $table->integer('user_gender_id')->unsigned()->nullable();
            $table->integer('user_category_id')->unsigned()->nullable();
            $table->integer('eye_color_id')->unsigned()->nullable();
            $table->integer('hair_color_id')->unsigned()->nullable();
            $table->integer('skin_color_id')->unsigned()->nullable();
            $table->integer('country_id')->unsigned()->nullable();
            $table->integer('residence_country_id')->unsigned()->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('user_gender_id')->references('id')->on('user_genders')->onDelete('cascade');
            $table->foreign('eye_color_id')->references('id')->on('user_eye_colors')->onDelete('cascade');
            $table->foreign('hair_color_id')->references('id')->on('user_hair_colors')->onDelete('cascade');            
            $table->foreign('skin_color_id')->references('id')->on('user_skin_colors')->onDelete('cascade');                        
        });

        Schema::create('users_accents', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('accent_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('accent_id')->references('id')->on('user_accents')->onDelete('cascade');
        });

        Schema::create('social_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->string('english_name')->index();
        });

        Schema::create('users_socials', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('social_media_id')->unsigned();
            $table->string('url');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('social_media_id')->references('id')->on('social_media')->onDelete('cascade');
        });

        Schema::create('user_portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title')->index();
            $table->longText('description', 5000)->nullable();
            $table->string('url')->index()->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
