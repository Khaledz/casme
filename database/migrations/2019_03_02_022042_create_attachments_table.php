<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachment_types', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('attachments', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('orginal_name');
            $table->string('description')->nullable();
            $table->string('model')->nullable();
            $table->integer('model_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->timestamps();

            // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('attachment_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
    }
}
