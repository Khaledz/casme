<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('bank_types', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name')->index();
        //     $table->timestamps();
        // });

        // Schema::create('banks', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('account_number')->index();
        //     $table->string('holder_name')->index();
        //     $table->integer('bank_type_id')->unsigned();
        //     $table->timestamps();

        //     $table->foreign('bank_type_id')->references('id')->on('bank_types')->onDelete('cascade');
        // });

        // Schema::create('transfer_types', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->string('name')->index();
        //     $table->timestamps();
        // });

        // Schema::create('transfers', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->integer('user_id')->unsigned();
        //     $table->integer('bank_id')->unsigned()->nullable();
        //     $table->integer('amount');
        //     $table->boolean('status')->default(0);
        //     $table->timestamps();
        // });

        Schema::create('payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::create('payment_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->timestamps();
        });

        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_code')->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('payment_status_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->integer('order_id')->unsigned()->nullable();
            $table->integer('amount');
            $table->timestamps();
        });

        Schema::create('balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('balance')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
