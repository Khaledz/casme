<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CountriesSeeder');
        $this->command->info('Seeded the countries!');
        // $this->call('ArabicCountry');
        // $this->command->info('Seeded the arabic countries!');
        $this->call('SetupSeeder');
        $this->call('PermissionSeeder');
        // factory(App\Models\User::class, 10)->create();
    }
}
