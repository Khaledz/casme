<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $crud = ['create', 'update', 'delete', 'read', 'in-sidebar'];
        $models = ['attachment', 'appSetting', 'user', 'category', 'menu', 'order', 'orderStatus', 'payment', 'priceType',
        'accent', 'eyeColor', 'hairColor', 'skinColor', 'portfolio', 'categoryRequirement'];
        
        foreach($crud as $action)
        {
            foreach($models as $model)
            {
                \DB::table('permissions')->insert([
                    'name' => "{$model}-{$action}",
                    'guard_name' => 'web'
                ]);
            }
        }

        // find admin
        $admin = \Spatie\Permission\Models\Role::find(1);
        foreach(\DB::table('permissions')->get() as $permission)
        {
            $admin->givePermissionTo($permission->name);
        }

    }
}
