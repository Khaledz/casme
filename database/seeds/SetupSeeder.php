<?php

use Illuminate\Database\Seeder;
use Stichoza\GoogleTranslate\GoogleTranslate;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('payment_methods')->insert([
            'name' => json_encode(['en' => 'Credit Card', 'ar' => 'فيزا كارد']),
        ]);

        \DB::table('payment_methods')->insert([
            'name' => json_encode(['en' => 'Paypal', 'ar' => 'باي بال']),
        ]);

        \DB::table('payment_methods')->insert([
            'name' => json_encode(['en' => 'Sadad', 'ar' => 'سداد']),
        ]);

        \DB::table('payment_statuses')->insert([
            'name' => json_encode(['en' => 'Pending', 'ar' => 'بإنتظار التأكيد']),
        ]);

        \DB::table('payment_statuses')->insert([
            'name' => json_encode(['en' => 'Failed', 'ar' => 'عملية فاشلة']),
        ]);

        \DB::table('payment_statuses')->insert([
            'name' => json_encode(['en' => 'Completed', 'ar' => 'عملية ناجحة']),
        ]);

        \DB::table('user_skin_colors')->insert([
            'name' => json_encode(['en' => 'Dark', 'ar' => 'أسمر']),
        ]);

        \DB::table('user_skin_colors')->insert([
            'name' => json_encode(['en' => 'White', 'ar' => 'أبيض']),
        ]);

        \DB::table('user_eye_colors')->insert([
            'name' => json_encode(['en' => 'Black', 'ar' => 'أسود']),
        ]);

        \DB::table('user_hair_colors')->insert([
            'name' => json_encode(['en' => 'Black', 'ar' => 'أسود']),
        ]);

        \DB::table('user_hair_colors')->insert([
            'name' => json_encode(['en' => 'Blonde', 'ar' => 'أشقر']),
        ]);

        \DB::table('category_types')->insert([
            'name' => json_encode(['en' => 'person', 'ar' => 'شخص']),
        ]);

        \DB::table('category_types')->insert([
            'name' => json_encode(['en' => 'item', 'ar' => 'قطعة']),
        ]);
        
        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Weight & height & Age', 'ar' => 'الطول والوزن والعمر']),
            'validations' => json_encode(
                [
                    'weight' => 'required|integer',
                    'height' => 'required|integer',
                    'age' => 'required|integer'
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Eye Color', 'ar' => 'لون العينين']),
            'validations' => json_encode(
                [
                    'eye_color_id' => 'required|exists:user_eye_colors,id',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Hair Color', 'ar' => 'لون الشعر']),
            'validations' => json_encode(
                [
                    'hair_color_id' => 'required|exists:user_hair_colors,id',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'hands pictures', 'ar' => 'صور اليدين']),
            'validations' => json_encode(
                [
                    'front' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'back' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'price range', 'ar' => 'النطاق السعري']),
            'validations' => json_encode(
                [
                    'price_type_id' => 'required|integer|exists:price_types,id',
                    'price' => 'required|integer'
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'introduction Video', 'ar' => 'فيديو تعريفي']),
            'validations' => json_encode(
                [
                    'video' => 'required|url',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Portfolio', 'ar' => 'معرض اﻷعمال']),
            'validations' => json_encode(
                [
                    'portfolio_title' => 'nullable|min:3',
                    'portfolio_url' => 'nullable|min:3',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Accents', 'ar' => 'اللهجات']),
            'validations' => json_encode(
                [
                    'accents' => 'nullable',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Travling outside country', 'ar' => 'السفر خارج البلد']),
            'validations' => json_encode(
                [
                    'is_travel' => 'required|integer',
                ])
        ]);

        \DB::table('category_requirements')->insert([
            'name' => json_encode(['en' => 'Skin Color', 'ar' => 'لون البشرة']),
            'validations' => json_encode(
                [
                    'skin_color_id' => 'required|exists:user_skin_colors,id',
                ])
        ]);

        \DB::table('price_types')->insert([
            'name' => json_encode(['en' => 'Item', 'ar' => 'قطعة']),
        ]);

        \DB::table('price_types')->insert([
            'name' => json_encode(['en' => 'Individual', 'ar' => 'الفرد']),
        ]);

        \DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => json_encode(['en' => "admin",'ar' => "مدير"]),
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'name' => 'supervisor',
            'display_name' => json_encode(['en' => "supervisor",'ar' => "مشرف"]),
            'guard_name' => 'web'
        ]);
        
        \DB::table('roles')->insert([
            'display_name' => json_encode(['en' => "beneficiary",'ar' => "مستفيد"]),
            'name' => 'beneficiary',
            'guard_name' => 'web'
        ]);

        \DB::table('roles')->insert([
            'display_name' => json_encode(['en' => "memebr",'ar' => "عضو"]),
            'name' => 'member',
            'guard_name' => 'web'
        ]);
        
        \DB::table('categories')->insert([
            'name' => json_encode(['en' => "Famous People",'ar' => "مشاهير"]),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => "Famous People VIP",'ar' => " VIP مشاهير"]),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Actors', 'ar' => 'ممثلين']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Actors VIP', 'ar' => 'ممثلين VIP']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Photographers', 'ar' => 'مدراء تصوير']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Models', 'ar' => 'موديل']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Models VIP', 'ar' => 'موديل VIP']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Camera', 'ar' => 'الكاميرات']),
            'category_type_id' => 2
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Lenses', 'ar' => 'عدسات التصوير']),
            'category_type_id' => 2
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Lighting', 'ar' => 'اﻹضاءة']),
            'category_type_id' => 2
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Accessories', 'ar' => 'اﻹكسسوارات']),
            'category_type_id' => 2
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Directors', 'ar' => 'مخرجين']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Scenario Writer', 'ar' => 'كاتبين سيناريو']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Sound Engineers', 'ar' => 'مهندسين صوت']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Photographers', 'ar' => 'مصورين']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Preparations Show', 'ar' => 'معدين']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Video Editing', 'ar' => 'مونتاج']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Graphics', 'ar' => 'جرافيكس']),
            'category_type_id' => 1
        ]);
        
        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Security guards', 'ar' => 'حراس أمن']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Organizers', 'ar' => 'منظمين']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Sellers', 'ar' => 'بائعين']),
            'category_type_id' => 1
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'DJ', 'ar' => 'دي جي']),
            'category_type_id' => 2
        ]);

        \DB::table('categories')->insert([
            'name' => json_encode(['en' => 'Makeup', 'ar' => 'مكياج']),
            'category_type_id' => 2
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => "Famous People",'ar' => "مشاهير"]),
            'category_id' => 1
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => "Famous People VIP",'ar' => " VIP مشاهير"]),
            'category_id' => 2
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Actors', 'ar' => 'ممثلين']),
            'category_id' => 3
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Actors VIP', 'ar' => 'ممثلين VIP']),
            'category_id' => 4
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Photographers', 'ar' => 'مدراء تصوير']),
            'category_id' => 5
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Models', 'ar' => 'موديل']),
            'category_id' => 6
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Models VIP', 'ar' => 'موديل VIP']),
            'category_id' => 7
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Equipment', 'ar' => 'المعدات']),
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Camera', 'ar' => 'الكاميرات']),
            'category_id' => 8,
            'parent_id' => 8
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Lenses', 'ar' => 'عدسات التصوير']),
            'category_id' => 9,
            'parent_id' => 8
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Lighting', 'ar' => 'اﻹضاءة']),
            'category_id' => 10,
            'parent_id' => 8
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Accessories', 'ar' => 'اﻹكسسوارات']),
            'category_id' => 11,
            'parent_id' => 8
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Services', 'ar' => 'الخدمات']),
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Directors', 'ar' => 'مخرجين']),
            'category_id' => 12,
            'parent_id' => 13
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Scenario Writer', 'ar' => 'كاتبين سيناريو']),
            'category_id' => 13,
            'parent_id' => 13
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Sound Engineers', 'ar' => 'مهندسين صوت']),
            'category_id' => 14,
            'parent_id' => 13
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Photographers', 'ar' => 'مصورين']),
            'category_id' => 15,
            'parent_id' => 13
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Preparations Show', 'ar' => 'معدين']),
            'category_id' => 16,
            'parent_id' => 13
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Video Editing', 'ar' => 'مونتاج']),
            'category_id' => 17,
            'parent_id' => 13
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Graphics', 'ar' => 'جرافيكس']),
            'category_id' => 18,
            'parent_id' => 13
        ]);
        
        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Events', 'ar' => 'الفعاليات']),
            'category_id' => 21
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Security guards', 'ar' => 'حراس أمن']),
            'category_id' => 19,
            'parent_id' => 21
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Organizers', 'ar' => 'منظمين']),
            'category_id' => 20,
            'parent_id' => 21
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Sellers', 'ar' => 'بائعين']),
            'category_id' => 21,
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'DJ', 'ar' => 'دي جي']),
            'category_id' => 22
        ]);

        \DB::table('menus')->insert([
            'name' => json_encode(['en' => 'Makeup', 'ar' => 'مكياج']),
            'category_id' => 23,
        ]);
        
        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 1
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 4
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 5
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 6
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 7
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 8
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 9
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 1,
            'category_requirement_id' => 10
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 2,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 2,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 2,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 2,
            'category_requirement_id' => 4
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 3,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 3,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 4,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 4,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 5,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 5,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 6,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 6,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 7,
            'category_requirement_id' => 1
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 7,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 7,
            'category_requirement_id' => 3
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 8,
            'category_requirement_id' => 1
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 8,
            'category_requirement_id' => 2
        ]);

        \DB::table('categories_requirements')->insert([
            'category_id' => 8,
            'category_requirement_id' => 3
        ]);

        \DB::table('user_genders')->insert([
            'name' => json_encode(['en' => 'Male', 'ar' => 'ذكر'])
        ]);
        
        \DB::table('user_genders')->insert([
            'name' => json_encode(['en' => 'Female', 'ar' => 'أنثى'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Genric Image', 'ar' => 'صورة عامة'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Picture of front hand side', 'ar' => 'صورة اليد اﻷمامية'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Picture of back hand side', 'ar' => 'صورة اليد الخلفية'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Introduction Video', 'ar' => 'فيديو تعريفي'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Portfolio', 'ar' => 'معرض اﻷعمال'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Avatar', 'ar' => 'صورة شخصية'])
        ]);

        \DB::table('attachment_types')->insert([
            'name' => json_encode(['en' => 'Category Background', 'ar' => 'صورة خلفية لقسم'])
        ]);

        \DB::table('order_statuses')->insert([
            'name' => json_encode(['en' => 'Peding Approval', 'ar' => 'بإنتظار مراجعة اﻹدارة'])
        ]);

        \DB::table('order_statuses')->insert([
            'name' => json_encode(['en' => 'Pending Payment', 'ar' => 'بإنتظار عملية الدفع'])
        ]);

        \DB::table('order_statuses')->insert([
            'name' => json_encode(['en' => 'Pending Payment Approval', 'ar' => 'بإنتظار تأكيد عملية الدفع'])
        ]);

        \DB::table('order_statuses')->insert([
            'name' => json_encode(['en' => 'Completed', 'ar' => 'مكتمل'])
        ]);

        \DB::table('order_statuses')->insert([
            'name' => json_encode(['en' => 'Canceled', 'ar' => 'ملغي'])
        ]);

        \DB::table('users')->insert([
            'fname' => 'Khaled',
            'lname' => 'Bawazir',
            'email' => 'devkhaledz@gmail.com',
            'password' => bcrypt('123456'),
            'user_gender_id' => 1,
            'country_id' => 4,
            'residence_country_id' => 4,
            'user_category_id' => 1,
            'email_verified_at' => \Carbon\Carbon::now()
        ]);

        \DB::table('newsletter_channels')->insert([
            'name' => json_encode(['en' => 'Email', 'ar' => 'بريد إلكتروني'])
        ]);
        
        \DB::table('user_accents')->insert([
            'name' => json_encode(['en' => 'Hijazi Accent', 'ar' => 'لهجة حجازية'])
        ]);
        
        \DB::table('user_accents')->insert([
            'name' => json_encode(['en' => 'Egyptian Accent', 'ar' => 'لهجة مصرية'])
        ]);

        \DB::table('social_media')->insert([
            'name' => 'فيسبوك',
            'english_name' => 'facebook'
        ]);

        \DB::table('social_media')->insert([
            'name' => 'انستقرام',
            'english_name' => 'instagram'
        ]);

        \DB::table('social_media')->insert([
            'name' => 'تويتر',
            'english_name' => 'twitter'
        ]);

        \DB::table('social_media')->insert([
            'name' => 'سناب شات',
            'english_name' => 'snapchat'
        ]);

        \DB::table('rates')->insert([
            'rate' => 1,
            'name' => json_encode(['en' => 'One Star', 'ar' => 'نجمة واحدة'])
        ]);

        \DB::table('rates')->insert([
            'rate' => 2,
            'name' => json_encode(['en' => 'Two Stars', 'ar' => 'نجمتين'])
        ]);

        \DB::table('rates')->insert([
            'rate' => 3,
            'name' => json_encode(['en' => 'Three Stars', 'ar' => 'ثلاثة نجوم'])
        ]);

        \DB::table('rates')->insert([
            'rate' => 4,
            'name' => json_encode(['en' => 'Four Stars', 'ar' => 'أربعة نجوم'])
        ]);

        \DB::table('rates')->insert([
            'rate' => 5,
            'name' => json_encode(['en' => 'Five Stars', 'ar' => 'خمسة نجوم'])
        ]);
        
        \App\Models\User::first()->assignRole(\Spatie\Permission\Models\Role::first());

        \DB::table('app_settings')->insert([
            'website_title' => json_encode(['en' => 'Application Name', 'ar' => 'اسم التطبيق']),
            'facebook' => '',
            'twitter' => '',
            'instagram' => '',
            'snapchat' => '',
            'copyright' => json_encode(['en' => '', 'ar' => '']),
            'about' => json_encode(['en' => 'Application Description', 'ar' => 'شرح التطبيق']),
        ]);
    }
}
