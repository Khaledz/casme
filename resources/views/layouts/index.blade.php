<!DOCTYPE html>
<html dir="rtl" lang="ar">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="assets/libraries/chartist/chartist.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/leaflet.css" rel="stylesheet" type="text/css">
	<link href="assets/css/leaflet.markercluster.css" rel="stylesheet" type="text/css">
	<link href="assets/css/leaflet.markercluster.default.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.2/css/bootstrap.min.css">
	<link rel="stylesheet"href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css">
	<link href="assets/css/villareal-turquoise.css" rel="stylesheet" type="text/css" id="css-primary">
	<!-- Latest compiled and minified CSS -->
	

	<link href="assets/css/custom.rtl.css" rel="stylesheet" type="text/css" id="css-primary">
	<link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">
	
    <title>الصفحة الرئيسية - عنوان الموقع</title>
</head>

<body class="cover-pull-top header-transparent">
<div class="page-wrapper">
				
    <div class="header-wrapper">
	<div class="header header-small">
		<div class="header-inner">
			<div class="container">
				
				<div class="header-top">
					<div class="header-top-inner">
						<a class="header-logo" href="index.html">
							<span class="header-logo-shape"></span> 
							<span class="header-logo-text">اللوقو</span>
						</a><!-- /.header-logo -->
						@if(auth()->guest())
						<a class="header-action" href="{{ url('/register')}}">
							<i class="fa fa-user"></i> <span>سجل حسابك اﻵن!</span>
						</a>
						@endif
						
<div class="nav-primary-wrapper collapse navbar-toggleable-sm">
	<ul class="nav nav-pills nav-primary">
		<li class="nav-item">
			<a href="#" class="nav-link active">
				الصفحة الرئيسية
			</a>	
		</li>

		@if(auth()->user())
		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				حسابي الشخصي
			</a>
			<ul class="sub-menu">
				<li><a href="{{ route('app.user.show', auth()->user())}}">عرض الملف الشخصي</a></li>
				<li><a href="{{ route('app.user.show', auth()->user())}}">اﻹعدادات</a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">تسجيل الخروج</a></li>
                <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
			</ul>		
		</li>
		@endif

		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				مشاهير
			</a>

			<ul class="sub-menu">
				<li><a href="{{ url('category/1') }}">مشاهير</a></li>
				<li><a href="{{ url('category/2') }}">مشاهير VIP</a></li>
			</ul>			
		</li>	
		
		<li class="nav-item nav-item-parent">
			<a href="" class="nav-link ">
				ممثلين
			</a>

			<ul class="sub-menu">
				<li><a href="{{ url('category/3') }}">ممثلين</a></li>
				<li><a href="{{ url('category/4') }}">ممثلين VIP</a></li>
			</ul>			
		</li>

		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				موديل
			</a>

			<ul class="sub-menu">
				<li><a href="{{ url('category/7') }}">موديل</a></li>
				<li><a href="{{ url('category/8') }}">موديل VIP</a></li>
			</ul>			
		</li>

		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				المعدات
			</a>

			<ul class="sub-menu">
				<li><a href="#">كاميرات</a></li>
				<li><a href="#">عدسات</a></li>
				<li><a href="#">إضاءة</a></li>
				<li><a href="#">إكسسوارات</a></li>
			</ul>			
		</li>

		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				الخدمات
			</a>

			<ul class="sub-menu">
				<li><a href="#">مخرجين</a></li>
				<li><a href="#">كتاب سيناريو</a></li>
				<li><a href="#">مهندسين صوت</a></li>
				<li><a href="#">مصورين</a></li>
				<li><a href="#">معدين</a></li>
				<li><a href="#">مونتاج</a></li>
				<li><a href="#">جرافيكس</a></li>
			</ul>			
		</li>
		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				الفعاليات
			</a>
			<ul class="sub-menu">
				<li><a href="#">حراس أمن</a></li>
				<li class="nav-item-parent">
					<a href="#">منظمين</a>

					<ul class="sub-menu">
						<li><a href="#">بائعين</a></li>
						<li><a href="#">دي جي</a></li>
					</ul>
				</li>
			</ul>			
		</li>
		@if(auth()->guest())
		<li class="nav-item nav-item-parent">
			<a href="#" class="nav-link ">
				منطقة اﻷعضاء
			</a>
			<ul class="sub-menu">
				<li><a href="/login">تسجيل دخول</a></li>
				<li><a href="/register">تسجيل حساب</a></li>
			</ul>			
		</li>
		@endif
	</ul><!-- /.nav -->
</div><!-- /.nav-primary -->


						<button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
	                        <span></span>
	                        <span></span>
	                        <span></span>
	                    </button>						
					</div><!-- /.header-top-inner -->
				</div><!-- /.header-top -->
			</div><!-- /.container -->
		</div><!-- /.header-inner -->
	</div><!-- /.header -->
</div><!-- /.header-wrapper-->
	    
    <div class="main-wrapper">
	    <div class="main">
	        <div class="main-inner">
	        	

	            <div class="content">
	                <div class="cover cover-center">
	<div class="cover-image"></div><!-- /.cover-image -->

	<div class="cover-title">
		<div class="container">
			<div class="cover-title-inner">
				<h1>نوفر إحتياجك لتبني نجاحك!</h1>

				<p> نوفر لك كل مايلزم لتبدأ رحلتك في بناء فريق إحترافي حسب التخصصات المطلوبة في مشروعك.</p>
			</div><!-- /.cover-title-->

			<div class="row">
				<div class="form-group col-md-3">
					<label class="white-color">القسم</label>
					{!! Form::select('categry_id', $categories, null, ['class' => 'form-control']) !!}
				</div><!-- /.form-group -->
	
				<div class="form-group col-md-3">
					<label class="white-color">الدولة</label>
					
					{!! Form::select('country_id', $countries, null, ['class' => 'form-control']) !!}
				</div><!-- /.form-group -->
	
				<div class="form-group col-md-3">
					<label class="white-color">الجنس</label>
					
					{!! Form::select('user_gender_id', $genders, null, ['class' => 'form-control']) !!}
				</div><!-- /.form-group -->
				<div class="form-group col-md-3">
					<label class="white-color">التقييم</label>
					
					{!! Form::select('rate_id', $rates, null, ['class' => 'form-control']) !!}
				</div><!-- /.form-group -->
	
				<div class="col-md-12">
					<div class="form-group-btn form-group-btn-placeholder-gap text-center">
						<button type="submit" class="btn btn-primary">إبدأ البحث</button>
					</div><!-- /.form-group -->	
				</div><!-- /.col-* -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.cover-title -->
</div><!-- /.cover -->


<div class="container">
	<div class="page-header">
		<h1>عن ماذا تبحث؟</h1> 
		<p>
			إختر من ضمن العديد من خدماتنا التي نسعد بتقديمها لكم.
		</p>
	</div><!-- /.page-header -->

		<div class="categories">
	<div class="row">
		<div class="col-sm-4">
			<div class="category" style="background-image: url('assets/img/tmp/tmp-6.jpg');">
				<a href="{{ route('app.category.show', 1) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">مشاهير</span>
						<span class="btn btn-primary">إستعرض</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->

		<div class="col-sm-4">
			<div class="category" style="background-image: url('assets/img/tmp/tmp-5.jpg');">
				<a href="{{ route('app.category.show', 3) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">ممثلين</span>
						<span class="btn btn-primary">إستعرض</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->

		<div class="col-sm-4">
			<div class="category" style="background-image: url('assets/img/tmp/tmp-4.jpg');">
				<a href="{{ route('app.category.show', 5) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">موديل</span>
						<span class="btn btn-primary">إستعرض</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->

		

		<div class="col-sm-4">
			<div class="category" style="background-image: url('assets/img/tmp/tmp-3.jpg');">
				<a href="{{ route('app.category.show', 5) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">معدات</span>
						<span class="btn btn-primary">إستعرض</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->

		<div class="col-sm-4">
			<div class="category" style="background-image: url('assets/img/tmp/tmp-10.jpg');">
				<a href="{{ route('app.category.show', 4) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">فعاليات</span>
						<span class="btn btn-primary">إستعرض</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->

		<div class="col-sm-4">
			<div class="category" style="background-image: url('assets/img/tmp/tmp-11.jpg');">
				<a href="{{ route('app.category.show', 3) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">خدمات</span>
						<span class="btn btn-primary">إستعرض</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->
	</div><!-- /.row -->
</div><!-- /.categories -->

	<div class="push-top">
		<div class="background-white fullwidth">
	<div class="boxes">
		<div class="row">
			<div class="col-md-6 col-lg-3">
				<div class="box">
					<div class="box-icon">
						<i class="fa fa-diamond"></i>
					</div><!-- /.box-icon -->

					<div class="box-content">
						<h2>خدمة إحترافية</h2>
						<p>نقدم لك خدمة إحترافية بتوفير لك كل مايلزم لتبدأ مشروعك.</p>
					</div><!-- /.box-content -->
				</div><!-- /.box -->
			</div><!-- /.col-* -->

			<div class="col-md-6 col-lg-3">
				<div class="box">
					<div class="box-icon">
						<i class="fa fa-star-o"></i>
					</div><!-- /.box-icon -->

					<div class="box-content">
						<h2>خدمات شاملة</h2>
						<p>نقدم خدمات متكاملة بجميع التخصصات.</p>
					</div><!-- /.box-content -->
				</div><!-- /.box -->
			</div><!-- /.col-* -->

			<div class="col-md-6 col-lg-3">
				<div class="box">
					<div class="box-icon">
						<i class="fa fa-cog"></i>
					</div><!-- /.box-icon -->

					<div class="box-content">
						<h2>نفهمك أكثر</h2>
						<p>ﻷننا نفهم إحتياجك، وفرنا لك جميع خصائص البحث الممكنة..</p>
					</div><!-- /.box-content -->
				</div><!-- /.box -->
			</div><!-- /.col-* -->

			<div class="col-md-6 col-lg-3">
				<div class="box">
					<div class="box-icon">
						<i class="fa fa-desktop"></i>
					</div><!-- /.box-icon -->

					<div class="box-content">
						<h2>نخدمك على جميع المنصات</h2>
						<p>تطبيقنا متوفر على جميع المنصات لنصل إليك، الويب والهاتف الذكي.</p>
					</div><!-- /.box-content -->
				</div><!-- /.box -->
			</div><!-- /.col-* -->
		</div><!-- /.row -->
	</div><!-- /.boxes -->
</div>

	</div>

</div><!-- /.container -->

<div class="container">
	<div class="page-header">
		<h1>شركاء النجاح</h1> 
	</div>
	<div class="partners">
	<div class="partner-wrapper">
		<a href="#"><img src="assets/img/tmp/partner.png" alt=""></a>
	</div><!-- /.partner-wrapper -->

	<div class="partner-wrapper">
		<a href="#"><img src="assets/img/tmp/partner.png" alt=""></a>
	</div><!-- /.partner-wrapper -->

	<div class="partner-wrapper">
		<a href="#"><img src="assets/img/tmp/partner.png" alt=""></a>
	</div><!-- /.partner-wrapper -->

	<div class="partner-wrapper">
		<a href="#"><img src="assets/img/tmp/partner.png" alt=""></a>
	</div><!-- /.partner-wrapper -->	

	<div class="partner-wrapper partner-wrapper-no-right-border">
		<a href="#"><img src="assets/img/tmp/partner.png" alt=""></a>
	</div><!-- /.partner-wrapper -->		
</div><!-- /.partners -->
</div><!-- /.container -->
	            </div><!-- /.content -->
	        </div><!-- /.main-inner -->
	    </div><!-- /.main -->
    </div><!-- /.main-wrapper -->


@include('partials.app.footer')
</div><!-- /.page-wrapper -->

<script src="//maps.googleapis.com/maps/api/js" type="text/javascript"></script>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script type="text/javascript" src="assets/js/jquery.ezmark.min.js"></script>
<script type="text/javascript" src="assets/js/tether.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/gmap3.min.js"></script>
<script type="text/javascript" src="assets/js/leaflet.js"></script>
<script type="text/javascript" src="assets/js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="assets/libraries/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/libraries/chartist/chartist.min.js"></script>
<script type="text/javascript" src="assets/js/scrollPosStyler.js"></script>
<script type="text/javascript" src="assets/js/villareal.js"></script>

</body>
</html>