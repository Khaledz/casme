<!DOCTYPE html>
@if(config('app.locale') == 'ar')
@php $dir= 'rtl'; @endphp
@elseif(config('app.locale') == 'en')
@php $dir= 'ltr'; @endphp
@endif
<html lang="ar" dir="{{ $dir }}">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>@yield('title') - {{ $app_setting->website_title }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!--begin::Global Theme Styles -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">
        <link href="/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/assets/libraries/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css">
        <link href="/assets/libraries/chartist/chartist.min.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/leaflet.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/leaflet.markercluster.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/leaflet.markercluster.default.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://getbootstrap.com/docs/4.2/dist/css/bootstrap.min.css">
        <link rel="stylesheet"href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css">
        <link href="/assets/css/villareal-turquoise.css" rel="stylesheet" type="text/css" id="css-primary">
        <link rel="shortcut icon" type="image/x-icon" href="/assets/favicon.png">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link href="/assets/css/social-buttons.css" rel="stylesheet" type="text/css" id="css-primary">    
        @if($dir == 'rtl')
        <link href="/assets/css/custom.rtl.css" rel="stylesheet" type="text/css" id="css-primary">
        @else
        <link href="/assets/css/custom.css" rel="stylesheet" type="text/css" id="css-primary">
        @endif
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
	<!-- end::Head -->
	<!-- begin::Body -->
	<body class="">
        <div class="page-wrapper" id="app">
            @include('partials.app.header')
            <div class="main-wrapper">
	            <div class="main">
	                <div class="main-inner">
                        @include('partials.app.breadcrumb')
                        <div class="content">
	                        <div class="container">
                            @include('partials.app.messages')
                            @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('partials.app.footer')
        </div>

        <!--begin::Global Theme Bundle -->
        <script type="text/javascript" src="/js/app.js"></script>        
        <script type="text/javascript" src="/assets/js/jquery.js"></script>
        <script type="text/javascript" src="/assets/js/jquery.ezmark.min.js"></script>
        <script type="text/javascript" src="/assets/js/tether.min.js"></script>
        <script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/gmap3.min.js"></script>
        <script type="text/javascript" src="/assets/js/leaflet.js"></script>
        <script type="text/javascript" src="/assets/js/leaflet.markercluster.js"></script>
        <script type="text/javascript" src="/assets/libraries/owl-carousel/owl.carousel.min.js"></script>
        <script type="text/javascript" src="/assets/libraries/chartist/chartist.min.js"></script>
        <script type="text/javascript" src="/assets/js/scrollPosStyler.js"></script>
        <script type="text/javascript" src="/assets/js/villareal.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script>
        // $(document).ready(function() {
        //     $('select').select2();
        // });
        </script>
		@yield('script')
    </body>
</html>