@extends('layouts.app')
@section('title')
تسجيل حساب مستفيد
@endsection
@section('content')
@php session()->put('role', 'beneficiary') @endphp

<div class="row">
    <div class="social-btn center-block">
        <div class="col-md-4">
            <a href="{{ url('/register-social-media/facebook') }}" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> تسجيل عبر <b>فيسبوك</b></a>
        </div>
        <div class="col-md-4">
            <a href="{{ url('/register-social-media/twitter') }}" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> تسجيل عبر <b>تويتر</b></a>
        </div>
        <div class="col-md-4">
            <a href="{{ url('/register-social-media/instagram') }}" class="btn btn-instagram btn-block"><i class="fa fa-instagram"></i> تسجيل عبر <b>إنستقرام</b></a>
        </div>
    </div>
</div>

<beneficiary-register inline-template>
<div class="row">
    <div class="col-md-12 col-lg-12" id="scrollToErrors">
        <ul class="alert alert-danger hide" v-bind:class="{ show: this.errors != null }" v-show="this.errors != null">
            <li v-for="(value, key, index) in this.errors" :key="index">@{{ value }}</li>
        </ul>
        <form method="post" ref="form" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-8 col-lg-8 offset-2">
                    <div class="page-header page-header-small">
                        <h3>المعلومات اﻷساسية</h3>
                    </div>

                    <div class="row">	
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">إختر فئتك</label>
                                {!! Form::select('category_id', $categories, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.user_category_id', '@change' => 'getCategoryRequirements']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">اﻹسم اﻷول</label>
                                {!! Form::text('fname', null, ['class' => 'form-control', 'v-model' => 'form.fname']) !!}
                            </div><!-- /.form-group -->
                                
                            <div class="form-group">
                                <label for="">كلمة المرور</label>
                                {!! Form::password('password', ['class' => 'form-control', 'v-model' => 'form.password']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">الجنس</label>
                                {!! Form::select('user_gender_id', $genders, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.user_gender_id']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">بلد اﻹقامة</label>
                                {!! Form::select('residence_country_id', $countries, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.residence_country_id']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">الجنسية</label>
                                {!! Form::select('country_id', $countries, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.country_id']) !!}
                            </div><!-- /.form-group -->

                        </div><!-- /.col-* -->
            
                        <div class="col-sm-6">	
                            <div class="form-group">
                                <label for="">البريد اﻹلكتروني</label>
                                {!! Form::text('email', null, ['class' => 'form-control', 'v-model' => 'form.email']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">اﻹسم اﻷخير</label>
                                {!! Form::text('lname', null, ['class' => 'form-control', 'v-model' => 'form.lname']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">تأكيد كلمة المرور</label>
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'v-model' => 'form.password_confirmation']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">رقم الجوال</label>
                                {!! Form::text('phone', null, ['class' => 'form-control', 'v-model' => 'form.phone']) !!}
                            </div><!-- /.form-group -->
                            
                            <div class="form-group">
                                <label for="">مدينة اﻹقامة</label>
                                {!! Form::text('city', null, ['class' => 'form-control', 'v-model' => 'form.city']) !!}
                            </div><!-- /.form-group -->	

                            <div class="form-group">
                                <label for="">صورة شخصية</label>
                                {!! Form::file('avatar', ['class' => 'form-control', 'ref' => 'avatar']) !!}
                            </div><!-- /.form-group -->	
                        </div><!-- /.col-* -->
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">نبذة عنك</label>
                                {!! Form::textarea('about', null, ['class' => 'form-control', 'v-model' => 'form.about']) !!}
                            </div>
                        </div>
                    </div><!-- /.row -->
                    
                    <div class="page-header page-header-small">
                        <h3>حسابات التواصل اﻹجتماعي</h3>
                    </div>
                    <div class="row">	
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">فيسبوك</label>
                                <input type="text" name="facebook" v-model="form.facebook" class="form-control" />
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="">إنستقرام</label>
                                <input type="text" name="instagram" v-model="form.instagram" class="form-control" />
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="">سناب شات</label>
                                <input type="text" name="snapchat" v-model="form.snapchat" class="form-control" />
                            </div><!-- /.form-group -->
                            <div class="form-group">
                                <label for="">تويتر</label>
                                <input type="text" name="twitter" v-model="form.twitter" class="form-control" />
                            </div><!-- /.form-group -->
                        </div>
                    </div><!-- /.row -->

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 1) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>معلومات إضافية عنك</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="">الطول (سم)</label>
                                    {!! Form::text('height', null, ['class' => 'form-control', 'v-model' => 'form.height']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="">الوزن (كج)</label>
                                    {!! Form::text('weight', null, ['class' => 'form-control', 'v-model' => 'form.weight']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="">العمر</label>
                                    {!! Form::text('age', null, ['class' => 'form-control', 'v-model' => 'form.age']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 3) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>لون الشعر</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">لون الشعر</label>
                                    {!! Form::select('hair_color_id', $hair_colors, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.hair_color_id']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 2) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>لون العينين</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">لون العينين</label>
                                    {!! Form::select('eye_color_id', $eye_colors, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.eye_color_id']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 10) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>لون البشرة</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">لون البشرة</label>
                                    {!! Form::select('skin_color_id', $skin_colors, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.skin_color_id']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 7) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>معرض اﻷعمال <small>تستطيع تعديل اﻷعمال لاحقاً</small></h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">عنوان العمل</label>
                                    {!! Form::text('portfolio_title', null, ['class' => 'form-control', 'v-model' => 'form.portfolio_title']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="">رابط العمل</label>
                                    {!! Form::text('portfolio_url', null, ['class' => 'form-control', 'v-model' => 'form.portfolio_url']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 8) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>أي من اللهجات تجيد التحدث بها؟</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    @foreach($accents as $accent)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="{{ $accent->id }}" v-model="form.accents">
                                        <label class="form-check-label" for="inlineCheckbox1">&nbsp;{{ $accent->name }}</label>
                                    </div>
                                    @endforeach
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 9) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>هل تستطيع السفر خارج البلد؟</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" v-model="form.is_travel">
                                        <label class="form-check-label" for="inlineRadio1">&nbsp;نعم</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="0" v-model="form.is_travel">
                                        <label class="form-check-label" for="inlineRadio2">&nbsp;لا</label>
                                    </div>
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 5) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>فئة النطاق السعري</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>نوع التسعيرة</label>
                                    {!! Form::select('price_type_id', $price_types, null, ['class' => 'form-control js-example-basic-single', 'v-model' => 'form.price_type_id']) !!}
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label for="">السعر</label>
                                    {!! Form::text('price', null, ['class' => 'form-control', 'v-model' => 'form.price']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 6) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>إرفاق رابط تسجيل فيديو تعرف فيه عن نفسك</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>ألصق رابط الفيديو التعريفي سواءا كان على اليوتيوب أو غيره من مواقع الفيديو</label>
                                    {!! Form::text('video', null, ['class' => 'form-control', 'v-model' => 'form.video']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>
                    
                    <div v-show="this.requirements.length > 0 && typeof this.requirements[0].find(requirement => requirement.id === 4) !== 'undefined'">
                        <div class="page-header page-header-small">
                            <h3>رفع صورة لكلاً من الجهة الخلفية واﻷمامية لليد</h3>
                        </div>
                        <div class="row">	
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>الجهة الخلفية لليد</label>
                                {!! Form::file('back', ['class' => 'form-control', 'ref' => 'back']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                <label>الجهة اﻷمامية لليد</label>
                                {!! Form::file('front', ['class' => 'form-control', 'ref' => 'front']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div><!-- /.row -->
                    </div>

                    <div class="page-header page-header-small">
                        <h3>إتفاقية اﻹستخدام</h3>
                    </div>
                    <div class="row">	
                        <div class="col-sm-12">
                            <div class="form-check form-check-inline">
                                <div class="ez-checkbox"><input class="form-check-input ez-hide" name="agreement" type="checkbox" id="inlineCheckbox1" v-model="form.agreement"></div>
                                <label class="form-check-label" for="inlineCheckbox1">&nbsp;لقد إطلعت على سياسة الخصوصية وشروط اﻹستخدام ووافقت عليها</label>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <br>
                    <div class="center">
                        <a href="#" @click.prevent="doRegister">
                            <button class="pricing-btn btn-block" v-bind:class="{ disabled: this.working }" type="submit">
                            <span v-if="this.working" class="spinner-border spinner-border-md" role="status" aria-hidden="true"></span>
                            سجل حسابي</button>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.col-* -->
</div><!-- /.row -->
</beneficiary-register>
@endsection