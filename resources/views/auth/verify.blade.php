@extends('layouts.app')
@section('title')
تفعيل الحساب
@endsection
@section('content')
<br>
<div class="col-lg-6 col-lg-offset-3">
    @if (session('resent'))
        <div class="alert alert-success" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-10">
                    <h4>تفعيل الحساب</h4>
                </div>
                <div class="col-md-2 text-right">
                </div>
            </div>
        </div>
        <div class="card-body">
            
            {{ __('Before proceeding, please check your email for a verification link.') }}
        {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
        </div>
    </div>
</div><!-- /.col-* -->
@endsection
