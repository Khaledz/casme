@extends('layouts.app')
@section('title')
نوع الحساب
@endsection
@section('content')
<div class="pricing-standard">
    

    <div class="pricing">
        <div class="page-header">
            <h2 class="text-center">إختر نوع الحساب:</h2>
        </div><!-- /.page-header -->
        <div class="row">
            <div class="pricing-col-wrapper col-lg-4 offset-md-1">
                <div class="pricing-col">
                    <h2 class="pricing-title">عضو</h2>

                    <div class="pricing-description">
                        للباحثين عن الخدمات
                    </div><!-- /.pricing-description -->

                    <ul class="pricing-features">
                        <li>التسجيل مجاني</li>
                        <li>البحث عن خدماتنا المتنوعة</li>
                        <li>فلترة اﻷشخاص حسب الرغبة</li>
                        <li>إرسال الطلبات إلكترونياً</li>
                        <li>دعم فني على مدار الساعة</li>			
                    </ul><!-- /.pricing-featured -->

                    <a href="{{ url('/register/member')}}"><button class="pricing-btn btn-block">إختر هذا الحساب</button></a>
                </div><!-- /.pricing-col -->
            </div><!-- /.pricing-col-wrapper-->
            <div class="pricing-col-wrapper col-lg-4  offset-md-1">
                <div class="pricing-col">
                    <h2 class="pricing-title">مستفيد</h2>

                    <div class="pricing-description">
                        لعارضين الخدمات
                    </div><!-- /.pricing-description -->

                    <ul class="pricing-features">
                        <li>التسجيل مجاني</li>
                        <li>تسويق خدماتك وعرضها للمشتركين</li>
                        <li>بروفايل مميز</li>
                        <li>تقييمات على خدماتك</li>
                        <li>دعم فني على مدار الساعة</li>			
                    </ul><!-- /.pricing-featured -->

                    <a href="{{ url('/register/beneficiary')}}"><button class="pricing-btn btn-block">إختر هذا الحساب</button></a>
                </div><!-- /.pricing-col -->
            </div><!-- /.pricing-col-wrapper -->        
        </div><!-- /.row -->
    </div><!-- /.pricing -->
</div>
@endsection
