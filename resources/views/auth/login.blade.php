@extends('layouts.app')
@section('title')
{{ __('general.login')}}
@endsection
@section('content')
<div class="row">
    <div class="col-md-4 col-lg-4 offset-4">
        <div class="page-header page-header-small">
            <h3>{{ __('general.login')}}</h3>
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <label for="">{{ __('general.email')}}</label>
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group">
                <label for="">{{ __('general.password')}}</label>
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group-btn">
                <button type="submit" class="btn btn-primary pull-right">{{ __('general.login')}}</button>
            </div>
            <div class="form-group-bottom-link">
                <a href="{{ route('password.request') }}">{{ __('general.forget_password')}} <i class="fa fa-long-arrow-left"></i></a>
            </div><!-- /.form-group-bottom-link -->
        </form>
    </div><!-- /.col-* -->
</div>
@endsection
