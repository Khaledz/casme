@extends('layouts.app')
@section('title')
حدد فئتك
@endsection
@section('content')
<div class="row">
    <div class="col-md-12 col-lg-12">
        
        <form method="post" action="{{ route('app.user.set_category') }}" enctype="multipart/form-data">
        @csrf
        
            <div class="row">
                <div class="col-md-8 col-lg-8 offset-2">
                    <div class="page-header page-header-small">
                        <h3>من فضلك حدد فئتك للمتابعة</h3>
                    </div>

                    <div class="row">	
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="">إختر فئتك</label>
                                {!! Form::select('category_id', $categories, null, ['class' => 'form-control js-example-basic-single']) !!}
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->
					</div>
                    <br>
                    <div class="center">
                    <a href="#">
                        <button class="pricing-btn btn-block"  type="submit">المتابعة</button>
                    </a>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.col-* -->
</div><!-- /.row -->
@endsection