@extends('layouts.app')
@section('title')
تسجيل حساب عضو
@endsection
@section('content')
@php session()->put('role', 'member') @endphp
<div class="row">
    <div class="social-btn center-block">
        <div class="col-md-4">
            <a href="{{ url('/register-social-media/facebook') }}" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> تسجيل عبر <b>فيسبوك</b></a>
        </div>
        <div class="col-md-4">
            <a href="{{ url('/register-social-media/twitter') }}" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> تسجيل عبر <b>تويتر</b></a>
        </div>
        <div class="col-md-4">
            <a href="{{ url('/register-social-media/instagram') }}" class="btn btn-instagram btn-block"><i class="fa fa-instagram"></i> تسجيل عبر <b>إنستقرام</b></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12">
        
        <form method="post" action="/register/member" enctype="multipart/form-data">
        @csrf
        
            <div class="row">
                <div class="col-md-8 col-lg-8 offset-2">
                    <div class="page-header page-header-small">
                        <h3>المعلومات اﻷساسية</h3>
                    </div>

                    <div class="row">	
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">اﻹسم اﻷول</label>
                                {!! Form::text('fname', null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->
                            
                            <div class="form-group">
                                <label for="">البريد اﻹلكتروني</label>
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->
    
                            <div class="form-group">
                                <label for="">كلمة المرور</label>
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">الجنس</label>
                                {!! Form::select('user_gender_id', $genders, null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">بلد اﻹقامة</label>
                                {!! Form::select('residence_country_id', $countries, null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->	
                        </div><!-- /.col-* -->
            
                        <div class="col-sm-6">	
                            <div class="form-group">
                                <label for="">اﻹسم اﻷخير</label>
                                {!! Form::text('lname', null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">رقم الجوال</label>
                                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->

                            <div class="form-group">
                                <label for="">تأكيد كلمة المرور</label>
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->
                            
                            <div class="form-group">
                                <label for="">الجنسية</label>
                                {!! Form::select('country_id', $countries, null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->		       
                        

                         <div class="form-group">
                                <label for="">المدينة</label>
                                {!! Form::text('city', null, ['class' => 'form-control']) !!}
                            </div><!-- /.form-group -->
                        </div><!-- /.col-* -->    

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">نبذة عنك</label>
                                    {!! Form::textarea('about', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <label for="">صورة شخصية</label>
                                    {!! Form::file('avatar', ['class' => 'form-control']) !!}
                                </div><!-- /.form-group -->	
                            </div>
                    </div><!-- /.row -->
                    
                    
                    <div class="page-header page-header-small">
                        <h3>إتفاقية اﻹستخدام</h3>
                    </div>
                    <div class="row">	
                        <div class="col-sm-12">
                            <div class="form-check form-check-inline">
                                <div class="ez-checkbox">{!! Form::checkbox('agreement', null, null, ['class' => 'form-check-input ez-hide']) !!}</div>
                                <label class="form-check-label" for="inlineCheckbox1">&nbsp;لقد إطلعت على سياسة الخصوصية وشروط اﻹستخدام ووافقت عليها</label>
                            </div>
                        </div>
                    </div><!-- /.row -->
                    <br>
                    <div class="center">
                    <a href="/register/member">
                        <button class="pricing-btn btn-block"  type="submit">سجل حسابي</button>
                    </a>
                    </div>
                </div>
            </div>
        </form>
    </div><!-- /.col-* -->
</div><!-- /.row -->
@endsection