<div class="header-wrapper">
	<div class="header header-small">
		<div class="header-inner">
			<div class="container">
                <div class="header-topbar">
					<div class="header-topbar-right">
						<ul class="nav nav-pills nav-topbar">

                            @if(config('app.locale') == 'ar')
                            <li class="nav-item ">
                                <a href="/lang/en" class="nav-link ">
                                    English
                                </a>
                            </li>
                            @elseif(config('app.locale') == 'en')
                            <li class="nav-item ">
                                <a href="/lang/ar" class="nav-link ">
                                    عربي
                                </a>
                            </li>
                            @endif
						</ul><!-- /.nav -->
					</div><!-- /.header-topbar-left -->

					<div class="header-topbar-left nav-topbar nav-topbar-social">
						<ul class="nav nav-pills">
							<li class="nav-item"><a href="{{ $app_setting->facebook }}" class="nav-link"><i class="fa fa-facebook"></i></a></li>
							<li class="nav-item"><a href="{{ $app_setting->twitter }}" class="nav-link"><i class="fa fa-twitter"></i></a></li>
							<li class="nav-item"><a href="{{ $app_setting->instagram }}" class="nav-link"><i class="fa fa-instagram"></i></a></li>
                        </ul><!-- /.nav -->
					</div><!-- /.header-topbar-right -->
				</div>
				<div class="header-top">
					<div class="header-top-inner">
						<a class="header-logo" href="{{ url('/') }}">
							<span class="header-logo-shape"></span>
							<span class="header-logo-text"></span>
						</a><!-- /.header-logo -->


<div class="nav-primary-wrapper collapse navbar-toggleable-sm">
        <ul class="nav nav-pills nav-primary">
                <li class="nav-item">
                    <a href="/" class="nav-link {{ \Request::segment(1) == '/' ? 'active' : '' }}">
                        {{ __('general.home_page')}}
                    </a>
                </li>

                @if(auth()->user() && auth()->user()->hasRole('admin'))
                <li class="nav-item nav-item-parent">
                    <a href="#" class="nav-link ">
                        {{ __('general.profile' )}}
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ url('/nova') }}">{{ __('general.control_panel' )}}</a></li>
                        <li><a href="{{ route('app.user.show', auth()->user())}}">{{ __('general.setting' )}}</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">{{ __('general.logout' )}}</a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
                @endif

                @if(auth()->user() && auth()->user()->hasRole('member'))
                <li class="nav-item nav-item-parent">
                    <a href="#" class="nav-link ">
                        حسابي الشخصي
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ route('app.user.dashboard')}}">{{ __('general.control_panel' )}}</a></li>
                        <li><a href="{{ route('app.user.order')}}">{{ __('general.order' )}}</a></li>
                        <li><a href="{{ route('app.user.payment')}}">{{ __('general.payment' )}}</a></li>
                        <li><a href="{{ route('app.user.setting')}}">{{ __('general.setting' )}}</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">{{ __('general.logout' )}}</a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
                @endif

                @if(auth()->user() && auth()->user()->hasRole('beneficiary'))
                <li class="nav-item nav-item-parent">
                    <a href="#" class="nav-link ">
                        {{ __('general.profile' )}}
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{ route('app.user.dashboard')}}">{{ __('general.control_panel' )}}</a></li>
                        <li><a href="{{ route('app.user.show', auth()->user())}}">{{ __('general.myprofile' )}}</a></li>
                        <li><a href="{{ route('app.user.portofolio')}}">{{ __('general.protofolio' )}}</a></li>
                        <li><a href="{{ route('app.user.payment')}}">{{ __('general.payment' )}}</a></li>
                        <li><a href="{{ route('app.user.setting')}}">{{ __('general.setting' )}}</a></li>
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">{{ __('general.logout' )}}</a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </li>
                @endif

                <li class="nav-item nav-item-parent">
                    <a href="#" class="nav-link ">
                        {{ __('general.service_menu' )}}
                    </a>

                    <ul class="sub-menu">
                        @foreach($menu as $parent)
                            @if(is_null($parent->parent) && $parent->children->count() == 0)
                            <li><a href="{{ route('app.category.show', $parent->category) }}">{{ $parent->name }}</a></li>
                            @elseif($parent->children->count() > 0)
                            <li class="nav-item-parent">
                                <a href="#" class="nav-link ">
                                {{ $parent->name }}
                                </a>
                                <ul class="sub-menu">
                                    @foreach($parent->children as $child)
                                    <li><a href="{{ route('app.category.show', $child->category) }}">{{ $child->name }}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </li>
                @if(auth()->guest())
                <li class="nav-item ">
                    <a href="/login" class="nav-link ">
                        {{ __('general.login' )}}
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="/register" class="nav-link ">
                        {{ __('general.register' )}}
                    </a>
                </li>
                @endif
                <li class="nav-item ">
                    <a href="{{ route('app.home.about') }}" class="nav-link ">
                        {{ __('general.about_us' )}}
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('app.home.contact') }}" class="nav-link ">
                        {{ __('general.contact_us' )}}
                    </a>
                </li>

</ul></div><!-- /.nav-primary -->


						<button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target=".nav-primary-wrapper">
	                        <span></span>
	                        <span></span>
	                        <span></span>
	                    </button>
					</div><!-- /.header-top-inner -->
				</div><!-- /.header-top -->
			</div><!-- /.container -->
		</div><!-- /.header-inner -->
	</div><!-- /.header -->
</div>