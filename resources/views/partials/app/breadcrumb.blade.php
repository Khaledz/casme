@if(! is_null(\Request::segment(1)))
<div class="content-title">
	<div class="content-title-inner">
		<div class="container">		
			<h1>@yield('title')</h1>		

			<ol class="breadcrumb">
				<li><a href="/">{{ __('general.home_page') }}</a></li>
				@if(\Route::has('app.'.\Request::segment(1).'.index'))
				<li><a href="#">{{ __('general.' . \Request::segment(1). '') }}</a></li>
				@endif
				<li><a href="">@yield('title')</a></li>
			</ol>			
		</div><!-- /.container -->
	</div><!-- /.content-title-inner -->
</div><!-- /.content-title -->
@endif