<div class="filter filter-secondary checkbox-light">
    
    {!! Form::model($model, ['route' => 'app.category.filter', 'method' => 'get']) !!}
        <div class="form-group">
            @php $all = [null => 'All']; @endphp
            <label>{{ __('general.category')}}</label>
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.country')}}</label>
            {!! Form::select('country_id', $all + $countries->toArray(), null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.city')}}</label>
            {!! Form::text('city', null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.gender')}}</label>
            {!! Form::select('gender_id', $all + $genders->toArray(), null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.rate')}}</label>
            {!! Form::select('rate_id', $all + $rates->toArray(), null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.skin_color')}}</label>
            {!! Form::select('skin_color', $all + $skin_colors->toArray(), null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.eye_color')}}</label>
            {!! Form::select('eye_color', array_merge([null => 'All'], $eye_colors->toArray()), null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="form-group">
            <label>{{ __('general.hair_color')}}</label>
            {!! Form::select('hair_color', $all + $hair_colors->toArray(), null, ['class' => 'form-control']) !!}
        </div><!-- /.form-group -->

        <div class="row">
            <div class="form-group col-xl-6">
                <label>{{ __('general.height_from')}}</label>
                {!! Form::text('height_from', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.to')}}</label>
                {!! Form::text('height_to', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.weight_from')}}</label>
                {!! Form::text('weight_from', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.to')}}</label>
                {!! Form::text('weight_to', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.age_from')}}</label>
                {!! Form::text('age_from', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.to')}}</label>
                {!! Form::text('age_to', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.price_from')}}</label>
                {!! Form::text('price_from', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->

            <div class="form-group col-xl-6">
                <label>{{ __('general.to')}}</label>
                {!! Form::text('price_to', null, ['class' => 'form-control']) !!}
            </div><!-- /.form-group -->		
        </div><!-- /.row -->

        <h2>{{ __('general.can_travel')}}</h2>

        <div class="checkbox">
            <label><div class="ez-radio">
                {!! Form::radio('is_travel', 1, null, ['class' => 'ez-hide']) !!}
            </div>
                <!-- <input type="radio" name="is_travel" value="1" class="ez-hide"></div>  -->
            {{ __('general.yes')}}</label>				
        </div><!-- /.checkbox -->

        <div class="checkbox">
            <label><div class="ez-radio">
                {!! Form::radio('is_travel', 0, null, ['class' => 'ez-hide']) !!}</div> 
            {{ __('general.no')}}</label>				
        </div><!-- /.checkbox -->

        <div class="checkbox">
            <label><div class="ez-radio">
                {!! Form::radio('is_travel', null, null, ['class' => 'ez-hide']) !!}
            </div> 
            {{ __('general.notcare')}}</label>
        </div><!-- /.checkbox -->

        <div class="form-group-btn form-group-btn-placeholder-gap">
            <button type="submit" class="btn btn-orange btn-block">{{ __('general.search')}}</button>
        </div><!-- /.form-group -->		
    </form>
</div><!-- /.filter -->	