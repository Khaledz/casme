<div class="page-header page-header-small">
	<h3>{{ __('general.price_range' )}}</h3>
</div>
<div class="row">	
	<div class="col-sm-12">
		<div class="form-group">
			<label>{{ __('general.price_type' )}}</label>
			{!! Form::select('price_type_id', $price_types, null, ['class' => 'form-control js-example-basic-single']) !!}
		</div><!-- /.form-group -->
		<div class="form-group">
			<label for="">{{ __('general.price' )}}</label>
			{!! Form::text('price', isset($user->price) ? $user->price->price : null, ['class' => 'form-control']) !!}
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->