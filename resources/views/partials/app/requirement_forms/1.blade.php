<div class="page-header page-header-small">
	<h3>{{ __('general.extra_information' )}}</h3>
</div>
<div class="row">	
	<div class="col-sm-4">
		<div class="form-group">
			<label for="">{{ __('general.height' )}}</label>
			{!! Form::text('height', null, ['class' => 'form-control']) !!}
		</div><!-- /.form-group -->
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="">{{ __('general.weight' )}}</label>
			{!! Form::text('weight', null, ['class' => 'form-control']) !!}
		</div><!-- /.form-group -->
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="">{{ __('general.age' )}}</label>
			{!! Form::text('age', null, ['class' => 'form-control']) !!}
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->