<div class="page-header page-header-small">
	<h3>{{ __('general.hair_color' )}}</h3>
</div>
<div class="row">	
	<div class="col-sm-12">
		<div class="form-group">
			<label for="">{{ __('general.hair_color' )}}</label>
			{!! Form::select('hair_color_id', $hair_colors, null, ['class' => 'form-control js-example-basic-single']) !!}
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->