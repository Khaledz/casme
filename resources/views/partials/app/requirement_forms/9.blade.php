<div class="page-header page-header-small">
	<h3>{{ __('general.can_travel')}}</h3>
</div>
<div class="row">	
	<div class="col-sm-12">
		<div class="form-group">
			<div class="form-check form-check-inline">
				{!! Form::radio('is_travel', 1, null, ['class' => 'form-check-input']) !!}
				<label class="form-check-label" for="inlineRadio1">&nbsp;{{ __('general.yes')}}</label>
			</div>
			<div class="form-check form-check-inline">
				{!! Form::radio('is_travel', 0, null, ['class' => 'form-check-input']) !!}
				<label class="form-check-label" for="inlineRadio2">&nbsp;{{ __('general.no')}}</label>
			</div>
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->