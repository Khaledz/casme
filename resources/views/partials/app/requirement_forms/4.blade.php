<div class="page-header page-header-small">
	<h3>{{ __('general.upload back and front picture of hands.' )}}</h3>
</div>
<div class="row">	
	<div class="col-sm-6">
		<div class="form-group">
		<label>{{ __('general.back_hand' )}} <img src="{{ $user->getBackHand() }}" width="40" height="40" class="rounded-circle" /></label>
		{!! Form::file('back', ['class' => 'form-control', 'ref' => 'back']) !!}
		<small>{{ __('general.password_empty' )}}</small>
		</div><!-- /.form-group -->
	</div>
	<div class="col-sm-6">
		<div class="form-group">
		<label>{{ __('general.front_hand' )}} <img src="{{ $user->getFrontHand() }}" width="40" height="40" class="rounded-circle" /></label>
		{!! Form::file('front', ['class' => 'form-control', 'ref' => 'front']) !!}
		<small>{{ __('general.password_empty' )}}</small>
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->