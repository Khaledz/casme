<div class="page-header page-header-small">
	<h3>{{ __('general.accents_you_speak')}}</h3>
</div>
<div class="row">	
	<div class="col-sm-12">
		<div class="form-group">
			@foreach($accents as $accent)
			<div class="form-check form-check-inline">
				{!! Form::checkbox('accents[]', $accent->id, $user->accents->contains($accent), ['class' => 'form-check-input']) !!}
				<label class="form-check-label" for="inlineCheckbox1">&nbsp;{{ $accent->name }}</label>
			</div>
			@endforeach
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->