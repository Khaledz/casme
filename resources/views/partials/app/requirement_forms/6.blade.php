<div class="page-header page-header-small">
	<h3>{{ __('general.upload_video')}}</h3>
</div>
<div class="row">	
	<div class="col-sm-12">
		<div class="form-group">
			<label>{{ __('general.paste_video_url')}}</label>
			{!! Form::text('video', null, ['class' => 'form-control']) !!}
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->