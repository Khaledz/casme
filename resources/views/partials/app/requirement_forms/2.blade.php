<div class="page-header page-header-small">
	<h3>{{ __('general.eye_color' )}}</h3>
</div>
<div class="row">	
	<div class="col-sm-12">
		<div class="form-group">
			<label for="">{{ __('general.eye_color' )}}</label>
			{!! Form::select('eye_color_id', $eye_colors, null, ['class' => 'form-control js-example-basic-single']) !!}
		</div><!-- /.form-group -->
	</div>
</div><!-- /.row -->