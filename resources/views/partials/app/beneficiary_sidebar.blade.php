<div class="dashboard-sidebar">
    <div class="dashboard-user hidden-md-down">
        <div class="dashboard-user-image" style="background-image: url({{$user->avatar()}});">
        </div>
        <strong>{{ $user->fullName }}</strong>
    </div>
    <div class="dashboard-nav-primary">
        <ul class="nav nav-stacked">
            <li class="nav-item">
                <a href="{{ route('app.user.dashboard')}}" class="nav-link {{ \Request::segment(2) == 'dashboard' ? 'active' : '' }}">
                    <i class="fa fa-building"></i> {{ __('general.control_panel')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('app.user.order') }}" class="nav-link {{ \Request::segment(2) == 'orders' || \Request::segment(1) == 'order' ? 'active' : '' }}">
                    <i class="fa fa-suitcase"></i> {{ __('general.order')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('app.user.portofolio') }}" class="nav-link {{ \Request::segment(2) == 'portofolios' || \Request::segment(1) == 'portofolio' ? 'active' : '' }}">
                    <i class="fa fa-suitcase"></i> {{ __('general.protofolio')}}
                </a>
            </li>						

            <li class="nav-item">
                <a href="{{ route('app.user.payment') }}" class="nav-link {{ \Request::segment(2) == 'payments' || \Request::segment(1) == 'setting' ? 'active' : '' }}">
                    <i class="fa fa-globe"></i> {{ __('general.payment')}}
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('app.user.setting') }}" class="nav-link {{ \Request::segment(2) == 'settings' || \Request::segment(1) == 'setting' ? 'active' : '' }}">
                    <i class="fa fa-users"></i> {{ __('general.setting')}}
                </a>
            </li>					
        </ul>
    </div><!-- /.dashboard-nav-primary -->

</div><!-- /.dashboard-sidebar -->
