<div class="footer-wrapper">
	<div class="container">
		<div class="footer-inner">
			<div class="row footer-top">
				<div class="col-md-6 ">
					<h2>{{ __('general.social_account')}}</h2>

					<div class="social">
						<a href="{{ $app_setting->facebook }}" targe="_blank"><i class="fa fa-facebook"></i></a>
						<a href="{{ $app_setting->twitter }}" targe="_blank"><i class="fa fa-twitter"></i></a>
						<!-- <a href="{{ $app_setting->snapchat }}" targe="_blank"><i class="fa fa-snapchat"></i></a> -->
						<a href="{{ $app_setting->instagram }}" targe="_blank"><i class="fa fa-instagram"></i></a>
					</div><!-- /.social -->									
				</div><!-- /.footer-top-left -->

				<div class="col-md-6 ">
					<h2>{{ $app_setting->website_title }}</h2>

					<p>
						{{ $app_setting->about }}
					</p>					
				</div><!-- /.footer-top-right -->
			</div><!-- /.footer-top -->

			<div class="footer-bottom">
				<div class="footer-left">
					&copy; {{ __('general.copyright')}} {{ $app_setting->website_title }} {{ date('Y') }}
				</div><!-- /.footer-left -->

				<div class="footer-right">
					<ul class="nav nav-pills">
						<li class="nav-item"><a href="{{ route('app.home.term') }}" class="nav-link">{{ __('general.terms')}}</a></li>
						<li class="nav-item"><a href="{{ route('app.home.privacy') }}" class="nav-link">{{ __('general.privacy')}}</a></li>
						<li class="nav-item"><a href="{{ route('app.home.contact') }}" class="nav-link">{{ __('general.contact_us')}}</a></li>
					</ul>
				</div><!-- /.footer-right -->			
			</div><!-- /.footer-bottom -->
		</div><!-- /.footer-inner -->
	</div><!-- /.container -->
</div><!-- /.footer-wrapper -->