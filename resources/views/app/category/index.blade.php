@extends('layouts.app')
@section('title')
{{ __('general.show') }} {{ __('general.category') }}
@endsection
@section('content')
<div class="page-header page-header-small">
    <h3>{{ __('general.show') }} {{ __('general.category') }}</h3>
</div><!-- /.page-header -->
<div class="categories">
	<div class="row">
	@if(count($categories) > 0)
		@foreach($categories as $category)
		@if(! is_null($category->attachment()->first()))
		<div class="col-sm-4">
			<div class="category" style="background-image: url({{  asset('storage/'. $category->attachment()->first()->name) }});">
				<a href="{{ route('app.category.show', $category->id) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">{{ $category->name }}</span>
						<span class="btn btn-primary">{{ __('general.browse') }}</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->
		@else
		<div class="col-sm-4">
			<div class="category" style="background-image: url();">
				<a href="{{ route('app.category.show', $category->id) }}" class="category-link">
					<span class="category-content">
						<span class="category-title">{{ $category->name }}</span>
						<span class="btn btn-primary">{{ __('general.browse') }}</span>
					</span><!-- /.category-content -->
				</a>
			</div><!-- /.category -->
		</div><!-- /.col-* -->
		@endif
		@endforeach
	@endif
	</div><!-- /.row -->
</div><!-- /.categories -->
@endsection