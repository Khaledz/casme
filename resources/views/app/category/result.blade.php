@extends('layouts.app')
@section('title')
{{ __('general.result') }}
@endsection
@section('content')
<div class="filter filter-gray push-bottom">
	<div class="row">
		<div class="col-md-4 col-lg-3">
            <div class="page-header page-header-small">
                <h3>{{ __('general.filter_result') }}</h3>
            </div><!-- /.page-header -->

            @include('partials.app.sidefilter')		
		</div><!-- /.col-* -->
		<div class="col-md-8 col-lg-9">
            <div class="page-header page-header-small">
                <h3>{{ __('general.result_count') }} ({{ $users->count() }}):</h3>
            </div><!-- /.page-header -->
			<div class="row">
            
                @if($users->count() == 0)
                    <p>{{ __('general.no_result') }}</p>
                @else
                @foreach($users as $user)
				<div class="col-md-6 col-lg-4">
					<div class="listing-box">
                        <div class="listing-box-image" style="background-image: url({{ $user->avatar() }})">
                            @if(! is_null($user->rate))
                            <div class="listing-box-image-label">
                                {{ $user->rate->rate->name }}
                            </div><!-- /.listing-box-image-label -->
                            @endif
                        </div><!-- /.listing-box-image -->
                        <div class="listing-box-title">
                            <span>{{ $user->fullName }}</span>
                            <!-- @if(! is_null($user->price))
                            <span class="pull-left">{{ $user->price->price}} ريال/{{ $user->price->type->name }}</span>
                            @endif -->
                            
                        </div><!-- /.listing-box-title -->
                        <div class="listing-box-content">
                            <dl>
                                <dt>{{ __('general.category') }}</dt><dd>{{ $user->category->name }}</dd>
                                <dt>{{ __('general.nationalty') }}</dt><dd>{{ $user->country->name }}</dd>
                                <dt>{{ __('general.residence') }}</dt><dd>{{ $user->country->name }}</dd>
                                <dt>{{ __('general.city') }}</dt><dd>{{ $user->city }}</dd>
                                <dt>{{ __('general.can_travel') }}</dt><dd>{{ $user->is_travel }}</dd>
                            </dl>
                            <a href="{{ route('app.user.show', $user)}}">
                                <button class="btn-primary btn-block btn-xs">{{ __('general.show_account') }}</button>
                            </a>
                        </div><!-- /.listing-box-cotntent -->
                    </div><!-- /.listing-box -->			
                </div><!-- /.col-* -->
                @endforeach
                @endif
			</div><!-- /.row -->		

            {{ $users->links() }}
            
		</div><!-- /.col-sm-* -->
    </div><!-- /.row -->
</div>
@endsection