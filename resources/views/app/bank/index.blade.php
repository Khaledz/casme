@extends('layouts.app')
@section('title')
حساب الحوالات البنكية
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-8  text-center font-weight-bold">
                <div class="address_recived text-center">
                    <h3 class="color2">
                    حساب الحوالات البنكية
</h3>

                    <hr class="hr">
                    <div class="row">
                    @if($banks->count() == 0)
                        لا يوجد حسابات مصرفية في الوقت الحالي.
                    @else
                    @foreach($banks as $bank)
                    <div class="col-12 color2 box ">
                        <div class="float-left">
                            <i class="fas fa-user-alt"></i>
                            <span>{{ $bank->holder_name }} ({{ $bank->account_number }})</span>

                        </div>
                        <div class=" float-right">
                        <span>{{ $bank->type->name }}</span>

                        </div>
                    </div>
                    @endforeach
                    @endif
                    </div>
                    <br>
                    <a href="{{ route('app.bank.create') }}" class="form-control btn btn-primary my-1 buttonbox col-6 ">إضافة حساب مصرفي</a>    
                </div></div>

            </div>
		</div>
	</div>
</div>
@endsection