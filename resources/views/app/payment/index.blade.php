@extends('layouts.app')
@section('title')
معرض اﻷعمال
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            @if(auth()->user()->hasRole('beneficiary'))
            @include('partials.app.beneficiary_sidebar')
            @elseif(auth()->user()->hasRole('member'))
            @include('partials.app.member_sidebar')
            @endif
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>الدفعات</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if($payments->count() == 0)
                        <p>لا يوجد بيانات مضافه في الوقت الحالي</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>كود الدفع</th>
                                <th>حالة الدفع</th>
                                <th>طريقة الدفع</th>
                                <th>المبلغ</th>
                                <th>طلب خدمات</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $payment)
                            <tr>
                                <td>{{ $payment->payment_code }}</td>
                                <td>{{ $payment->status->name }}</td>
                                <td>{{ $payment->method->name }}</td>
                                <td>{{ $payment->amount }} ريال سعودي</td>
                                <td><a href="{{ route('app.order.show', $payment->order) }}">{{ $payment->order->beneficiary->fullName }}</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
            <div class="text-center">
                {{ $payments->links() }}
            </div>
        </div>
    </div>
@endsection