@extends('layouts.app')
@section('title')
معرض اﻷعمال
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('partials.app.beneficiary_sidebar')
        </div>
        <div class="col-md-9">
        <br>
            <h5><a href="{{ route('app.portofolio.create') }}"><i class="fa fa-plus"></i> أضف عمل جديد</a></h5>
            <br>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>معرض اﻷعمال</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    
                    @if($portofolios->count() == 0)
                        <p>لا يوجد بيانات مضافه في الوقت الحالي</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>عنوان العمل</th>
                                <th>وصف العمل</th>
                                <th>رابط العمل</th>
                                <th>أدوات</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($portofolios as $portofolio)
                            <tr>
                                <td>{{ $portofolio->id }}#</td>
                                <td>{{ $portofolio->title }}</td>
                                <td>{{ $portofolio->description }}</td>
                                <td>
                                    @if(is_null($portofolio->url))
                                        -
                                    @else
                                        <a href="{{ $portofolio->url }}" target="_blank">عرض</a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('app.portofolio.edit', $portofolio) }}" class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="{{ route('app.portofolio.show', $portofolio) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
            <div class="text-center">
                {{ $portofolios->links() }}
            </div>
        </div>
    </div>
@endsection