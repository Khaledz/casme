@extends('layouts.app')
@section('title')
إضافة عمل جديد
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('partials.app.beneficiary_sidebar')
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>إضافة عمل جديد</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                <form method="post" action="{{ route('app.portofolio.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">	
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">عنوان العمل*</label>
                                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">وصف العمل</label>
                                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">رابط العمل*</label>
                                    {!! Form::text('url', null, ['class' => 'form-control']) !!}
                                </div><!-- /.form-group -->
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">صورة للعمل*</label>
                                    {!! Form::file('attachment', null, ['class' => 'form-control']) !!}
                                </div><!-- /.form-group -->
                            </div>
                        </div>
                        <br>
                        <div class="center">
                            <a href="">
                                <button class="pricing-btn btn-block"  type="submit">حفظ</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection