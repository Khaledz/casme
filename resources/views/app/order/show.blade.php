@extends('layouts.app')
@section('title')
عرض الطلب #{{ $order->id }}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('partials.app.beneficiary_sidebar')
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>عرض الطلب #{{ $order->id }}</h4>
                        </div>
                        <div class="col-md-2 text-right">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>حالة الطلب</th>
                                <th>السعر</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $order->id }}#</td>
                                <td>{{ $order->status()->first()->name }}</td>
                                <td>{{ is_null($order->price) ? 'قيد التسعير' : $order->price . ' ريال سعودي'}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>الخدمة المطلوبة</h4>
                        </div>
                        <div class="col-md-2 text-right">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>طلب خدمات</th>
                                <th>قسم</th>
                                <th>ملاحظات</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $order->beneficiary->fullName }}</td>
                                <td>{{ $order->beneficiary->category->name }}</td>
                                <td>{{ $order->message ?: '-'}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if(! is_null($order->price) && $order->status()->first()->id == 2)
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>الدفع</h4>
                        </div>
                        <div class="col-md-2 text-right">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('app.payment.store' )}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">

                        </div>
                        <input type="hidden" name="order_id" value="{{ $order->id }}" />
                        <br>
                        <div class="center">
                            <a href="#">
                                <button class="pricing-btn btn-block"  type="submit">الدفع بواسطة باي بال</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </div>
@endsection