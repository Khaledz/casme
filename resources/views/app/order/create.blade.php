@extends('layouts.app')
@section('title')
طلب جديد
@endsection
@section('content')
<div class="account ">
    <div class="container ">
        <div class="row">
			@include('partials.app.account_sidebar')
			<div class="col-md-8 text-center">
                        <div class="new_order container ">
                            <div class="fee " >
                                <div class="delivery float-left">
                                        <span class="float-left text-left">سعر التوصيل</span>
                                        <span class="float-right text-right numberfont">35</span>
                                </div>
                                <div class="delivery float-right">
                                            <span class=" float-left text-left">سعرالشحن</span>
                                            <span class="float-right text-right numberfont">50</span>
                                </div>
                            </div>

                            <div class="new_order_menue ">
                                <h3 class="color2">عنوان المرسل</h3>
                                <hr class="hr">
                                <div class="row row1 row11">
                                        <div class=" text-center   background2 color3">عنوانى</div>
                                        <div class=" text-center  now background2 color3">عنوان جديد</div>
                                        <div class=" text-center  background2 color3">إختيار عنوان</div>
                                </div>
                                <form>
                                        <div class="form-group row">
                                            <label for="inputPassword" class="col-4 col-lg-3 col-form-label">الاسم</label>
                                            <div class="col-8 col-lg-9 col-sm-8">
                                                <input type="text" class="form-control boxofinput "  >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputState" class="col-4 col-lg-3  col-form-label">المدينة</label>
                                            <div class="col-8 col-lg-9 col-sm-8">
                                                <select id="inputState" class="form-control boxofinput">
                                                  <option selected>جدة</option>
                                                  <option>...</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">الحى</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <input type="text" class="form-control boxofinput"  >
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">الموبايل</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <input type="text" class="form-control boxofinput"  >
                                                </div>
                                        </div>
                                        <div class="form-group form-check row">
                                                <div class=" col-1 text-left">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck1"  >
                                                </div>
                                                <label class="form-check-label col-11" for="exampleCheck1">حفظ العنوان لإستخدامه بالمستقبل</label>
                                        </div>
                                </form>
                                <h3 class="color2">الوقت المفضل للاستلام من المرسل</h3>
                                <hr class="hr">
                                <div class="row row1 row12">
                                            <div class=" text-center   background2 color3">أقرب وقت</div>
                                            <div class=" text-center  now background2 color3">اليوم</div>
                                            <div class=" text-center  background2 color3">غدا</div>
                                </div>
                                <form class="form-form-check">
                                        <div class="row form-check">
                                                <input type="radio" name="gender" value="male" checked class="col-1">
                                                <span class="col-2 ">اليوم</span>
                                                <span class="col-4" >من <span class="numberfont">10.00</span> صباحا</span>
                                                <span class="col-4 ">الى <span class="numberfont">10.00</span> مساء</span> <br>
                                        </div>
                                        <div class="row form-check ">
                                                <input type="radio" name="gender" value="male" class="col-1"  >
                                                <span class="col-2">اليوم</span>
                                                <span class="col-4 ">من <span class="numberfont">10.00</span> صباحا</span>
                                                <span class="col-4 ">الى <span class="numberfont">10.00</span> مساء</span> <br>
                                        </div>
                                        <div class="row form-check">
                                                <input type="radio" name="gender" value="male"class="col-1" >
                                                <span class="col-2" >اليوم</span>
                                                <span class="col-4 ">من <span class="numberfont">10.00</span> صباحا</span>
                                                <span class="col-4 ">الى <span class="numberfont">10.00</span> مساء</span> <br>
                                        </div>
                               
                                </form>
                                <h3 class="color2">عنوان المستلم</h3>
                                <hr class="hr">
                                <div class="row row1 row13">
                                                <div class=" text-center   background2 color3">عنوانى</div>
                                                <div class=" text-center now  background2 color3">عنوان جديد</div>
                                                <div class=" text-center  background2 color3">إختيار عنوان</div>
                                </div>
                                <form>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">الاسم</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <input type="text" class="form-control boxofinput "  >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputState" class="col-4 col-lg-3  col-form-label">المدينة</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <select id="inputState" class="form-control boxofinput">
                                                      <option selected>جدة</option>
                                                      <option>...</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                    <label for="inputPassword" class="col-4 col-lg-3 col-form-label">الحى</label>
                                                    <div class="col-8 col-lg-9 col-sm-8">
                                                        <input type="text" class="form-control boxofinput"  >
                                                    </div>
                                            </div>
                                            <div class="form-group row">
                                                    <label for="inputPassword" class="col-4 col-lg-3 col-form-label">الموبايل</label>
                                                    <div class="col-8 col-lg-9 col-sm-8">
                                                        <input type="text" class="form-control boxofinput"  >
                                                    </div>
                                            </div>
                                            <div class="form-group form-check row">
                                                    <div class=" col-1 text-left">
                                                            <input type="checkbox" class="form-check-input" id="exampleCheck1" >
                                                    </div>
                                                    <label class="form-check-label col-11" for="exampleCheck1">حفظ العنوان لإستخدامه بالمستقبل</label>
                                            </div>
                                </form>
                                <h3 class="color2">تحصيل قيمة البضاعة من المستلم</h3>
                                <hr class="hr">
                                <form>
                                    <div class="form-group row">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">قيمة البضاعة</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <input type="text" class="form-control fontaral boxofinput"  >
                                                </div>
                                    </div>
                                    <p class="text-left precive">- هذا هو المبلغ الذي سيتم إرجاعه لك بعد إستلامة .</p>
                                    <p class="text-left precive">- يرجى عدم إضافة قيمة التوصيل على قيمة البضاعة ما عدى في حال الدفع على الحساب .</p>
                                </form>
                                <h3 class="color2">معلومات إضافية ( غير إلزامية )</h3>
                                <hr class="hr">
                                <form>
                                    <div class="form-group row">
                                                <label for="inputPassword" class="col-4 col-lg-3 col-form-label">رقم الطلب</label>
                                                <div class="col-8 col-lg-9 col-sm-8">
                                                    <input type="text" class="form-control boxofinput" placeholder="اختيارى">
                                                </div>
                                    </div>
                                    <div class="form-group form-check row">
                                            <div class=" col-1 text-left">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1"  >
                                            </div>
                                            <label class="form-check-label col-11" for="exampleCheck1">تغليف الشحنة داخل كرتون ( <span class="numberfont">5</span> ريال )</label>
                                    </div>
                                    <div class="form-group form-check row">
                                            <div class=" col-1 text-left">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1"  >
                                            </div>
                                            <label class="form-check-label col-11" for="exampleCheck1">إضافة ملصق قابل للكسر</label>
                                    </div>
                                
                                </form>
                                <button type="submit" class="form-control btn btn-primary my-1 buttonbox maxwidth200px">التالى</button>



                            </div>
                        </div>
                      </div>
		</div>
	</div>
</div>
@endsection