@extends('layouts.app')
@section('title')
عرض الطلبات
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('partials.app.'. $user->roles()->first()->name .'_sidebar')
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>آخر الطلبات</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    @if($orders->count() == 0)
                        <p>لا يوجد بيانات مضافه في الوقت الحالي</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>حالة الطلب</th>
                                <th>السعر</th>
                                <th>عرض</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{ $order->id }}#</td>
                                <td>{{ $order->status()->first()->name }}</td>
                                <td>{{ is_null($order->price) ? 'قيد التسعير' : $order->price . ' ريال سعودي'}}</td>
                                <td>
                                    <a href="{{ route('app.order.show', $order) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
            <div class="text-center">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection