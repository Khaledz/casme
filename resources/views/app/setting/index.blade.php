@extends('layouts.app')
@section('title')
{{ __('general.setting') }}
@endsection
@section('content')
<div class="row">
    <div class="col-md-3">
        @include('partials.app.'. $user->roles()->first()->name .'_sidebar')
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-10">
                        <h4>{{ __('general.setting') }}</h4>
                    </div>
                </div>
            </div>
            <div class="card-body">
                {!! Form::model($user, ['route' => ['app.user.setting', $user], 'enctype' => 'multipart/form-data']) !!}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="page-header page-header-small">
                                <h3>{{ __('general.main_information') }}</h3>
                            </div>
                            <div class="row">	
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">{{ __('general.fname') }}</label>
                                        {!! Form::text('fname', null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->
                                    
                                    @if(auth()->user()->hasRole('beneficiary'))
                                    <div class="form-group">
                                        <label for="">{{ __('general.category') }}</label>
                                        @if(is_null($user->category))
                                            @php $category = null; @endphp
                                            {!! Form::select('user_category_id', $categories, $category, ['class' => 'form-control js-example-basic-single']) !!}
                                        @else
                                            @php $category = $user->category->id; @endphp
                                            {!! Form::select('user_category_id', $categories, $category, ['class' => 'form-control js-example-basic-single', 'disabled' ]) !!}
                                        @endif
                                        
                                    </div><!-- /.form-group -->
                                    @endif

                                    <div class="form-group">
                                        <label for="">{{ __('general.gender') }}</label>
                                        {!! Form::select('user_gender_id', $genders, null, ['class' => 'form-control js-example-basic-single']) !!}
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label for="">{{ __('general.password') }}</label> <small>{{ __('general.password_empty') }}</small>
                                        {!! Form::password('password', ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->


                                    <div class="form-group">
                                        <label for="">{{ __('general.residence') }}</label>
                                        {!! Form::select('residence_country_id', $countries, null, ['class' => 'form-control js-example-basic-single']) !!}
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label for="">{{ __('general.nationality') }}</label>
                                        {!! Form::select('country_id', $countries, null, ['class' => 'form-control js-example-basic-single']) !!}
                                    </div><!-- /.form-group -->

                                </div><!-- /.col-* -->
                    
                                <div class="col-sm-6">	
                                    
                                    <div class="form-group">
                                        <label for="">{{ __('general.lname') }}</label>
                                        {!! Form::text('lname', null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label for="">{{ __('general.email') }}</label>
                                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label for="">{{ __('general.password_conf') }}</label>
                                        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->

                                    <div class="form-group">
                                        <label for="">{{ __('general.phone') }}</label>
                                        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->
                                    
                                    <div class="form-group">
                                        <label for="">{{ __('general.city') }}</label>
                                        {!! Form::text('city', null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->	

                                    <div class="form-group">
                                        <label for="">{{ __('general.avatar') }} <img src="{{ $user->avatar() }}" width="40" height="40" class="rounded-circle" /></label>
                                        {!! Form::file('avatar', ['class' => 'form-control']) !!}
                                        <small>{{ __('general.avatar_empty') }}</small>
                                    </div><!-- /.form-group -->	
                                </div><!-- /.col-* -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">{{ __('general.about') }}</label>
                                        {!! Form::textarea('about', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div><!-- /.row -->
                            
                            @if(auth()->user()->hasRole('beneficiary'))
                            <div class="page-header page-header-small">
                                <h3>{{ __('general.social_account') }}</h3>
                            </div>
                            <div class="row">	
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">{{ __('general.facebook') }}</label>
                                        {!! Form::text('facebook', $user->socials->find(1) != null ? $user->socials->find(1)->pivot->url : null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="">{{ __('general.instagram') }}</label>
                                        {!! Form::text('instagram', $user->socials->find(2) != null ? $user->socials->find(2)->pivot->url : null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="">{{ __('general.snapchat') }}</label>
                                        {!! Form::text('snapchat', $user->socials->find(4) != null ? $user->socials->find(4)->pivot->url : null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                        <label for="">{{ __('general.twitter') }}</label>
                                        {!! Form::text('twitter', $user->socials->find(3) != null ? $user->socials->find(3)->pivot->url : null, ['class' => 'form-control']) !!}
                                    </div><!-- /.form-group -->
                                </div>
                            </div><!-- /.row -->
                            @if(auth()->user()->hasRole('beneficiary') && ! is_null($user->category))
                            @foreach($user->category->requirements as $requirement)
                            @if(View::exists('partials.app.requirement_forms.'. $requirement->id))
                            @include('partials.app.requirement_forms.'. $requirement->id)
                            @endif
                            @endforeach
                            @endif
                            @endif
                        </div>

                        <!-- vue -->
                        
                        <div class="center">
                            <a href="#">
                                <button class="pricing-btn btn-block"type="submit">
                                {{ __('general.save_setting') }}</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection