@extends('layouts.app')
@section('title')
{{ __('general.home_page')}}
@endsection
@section('content')
<flash-message variant="success"></flash-message>
	<div class="page-header">
		<h1>{{ $app_setting->website_title }}</h1>
		<p>
			{{ $app_setting->about }}
		</p>
	</div><!-- /.page-header -->

	<div class="categories">
		<div class="row">
		@if(count($categories) > 0)
			@foreach($categories as $category)
			@if(! is_null($category->attachment()->first()))
			<div class="col-sm-4">
				<div class="category" style="background-image: url({{  asset('storage/'. $category->attachment()->first()->name) }});">
					<a href="{{ route('app.category.show', $category->id) }}" class="category-link">
						<span class="category-content">
							<span class="category-title">{{ $category->name }}</span>
							<span class="btn btn-primary">{{ __('general.browse') }}</span>
						</span><!-- /.category-content -->
					</a>
				</div><!-- /.category -->
			</div><!-- /.col-* -->
			@else
			<div class="col-sm-4">
				<div class="category" style="background-image: url();">
					<a href="{{ route('app.category.show', $category->id) }}" class="category-link">
						<span class="category-content">
							<span class="category-title">{{ $category->name }}</span>
							<span class="btn btn-primary">{{ __('general.browse') }}</span>
						</span><!-- /.category-content -->
					</a>
				</div><!-- /.category -->
			</div><!-- /.col-* -->
			@endif
			@endforeach
		@endif
		</div><!-- /.row -->
	</div><!-- /.categories -->

	<div class="push-top">
		<div class="background-white fullwidth">
		<div class="boxes">
			<div class="row">
				<div class="col-md-6 col-lg-3">
					<div class="box">
						<div class="box-icon">
							<i class="fa fa-diamond"></i>
						</div><!-- /.box-icon -->

						<div class="box-content">
							<h2>{{ __('general.professional_service') }}</h2>
							<p>{{ __('general.professional_service_desc') }}</p>
						</div><!-- /.box-content -->
					</div><!-- /.box -->
				</div><!-- /.col-* -->

				<div class="col-md-6 col-lg-3">
					<div class="box">
						<div class="box-icon">
							<i class="fa fa-star-o"></i>
						</div><!-- /.box-icon -->

						<div class="box-content">
							<h2>{{ __('general.comprehensive') }}</h2>
							<p>{{ __('general.comprehensive_desc') }}</p>
						</div><!-- /.box-content -->
					</div><!-- /.box -->
				</div><!-- /.col-* -->

				<div class="col-md-6 col-lg-3">
					<div class="box">
						<div class="box-icon">
							<i class="fa fa-cog"></i>
						</div><!-- /.box-icon -->

						<div class="box-content">
							<h2>{{ __('general.understand_more') }}</h2>
							<p>{{ __('general.understand_more_desc') }}</p>
						</div><!-- /.box-content -->
					</div><!-- /.box -->
				</div><!-- /.col-* -->

				<div class="col-md-6 col-lg-3">
					<div class="box">
						<div class="box-icon">
							<i class="fa fa-desktop"></i>
						</div><!-- /.box-icon -->

						<div class="box-content">
							<h2>{{ __('general.platforms') }}</h2>
							<p>{{ __('general.platforms_desc') }}</p>
						</div><!-- /.box-content -->
					</div><!-- /.box -->
				</div><!-- /.col-* -->
			</div><!-- /.row -->
		</div><!-- /.boxes -->
	</div>
</div>

@endsection