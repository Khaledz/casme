@extends('layouts.app')
@section('title')
تواصل معنا
@endsection
@section('content')
<div class="container">
	<div class="row">	
		<div class="col-md-8 col-lg-9">
			<div class="page-subheader page-subheader-big">
				<h3>نموذج التواصل معنا</h3>
			</div>

			<div class="row">
				<form method="post" action="{{ route('app.home.contact') }}">
                @csrf

                    <div class="col-sm-6">
						<div class="form-group">
							<label>البريد اﻹلكتروني</label>
							{!! Form::text('email', null, ['class' => 'form-control']) !!}
						</div><!-- /.form-group -->
					</div><!-- /.col-* -->

					<div class="col-sm-6">
						<div class="form-group">
							<label>اﻹسم</label>
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
						</div><!-- /.form-group -->
					</div><!-- /.col-* -->

					<div class="col-sm-12">
						<div class="form-group">
							<label>نص الرسالة</label>
                            {!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 5]) !!}
						</div><!-- /.form-control -->
					</div><!-- /.col-* -->

					<div class="col-sm-12">
						<button type="submit" class="btn btn-primary pull-right">أرسل رسالتك</button>
					</div><!-- /.col-* -->
				</form>
			</div><!-- /.row -->
    	</div><!-- /.col-* -->

	    <div class="col-md-4 col-lg-3">
	    	<div class="sidebar page-subheader page-subheader-big">    			
				<div class="widget ">
	<h2 class="widgettitle">معلومات التواصل</h2>

	<table class="contact">
		<tbody>
			<tr>
				<th>العنوان:</th>
				<td>العنوان هنا</td>
			</tr>

			<tr>
				<th>رقم هاتف:</th>
				<td>+0-123-456-789</td>
			</tr>

			<tr>
				<th>Skype:</th>
				<td>سكايب</td>
			</tr>
		</tbody>
	</table>	
</div><!-- /.widget -->
	    	</div><!-- /.sidebar -->
	    </div><!-- /.col-* -->
	</div><!-- /.row -->    	
</div>
@endsection