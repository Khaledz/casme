@extends('layouts.app')
@section('title')
{{ $user->fullName }}
@endsection
@section('content')
<div class="row">
	<!-- show profile card -->
	@include('app.user.card')

	<div class="listing-detail col-md-8 col-lg-9">
		<div class="listing-user widget-background-white">
			<div class="row">
				<div class="col-md-8 col-lg-9">
					<div class="listing-user-image">
						<a href="#" style="background-image: url({{ $user->avatar() }});"></a>
					</div><!-- /.listing-user-image -->

					<div class="listing-user-title">
						<h2>{{ $user->fullName }}</h2>
						<h3>{{ $user->category->name }}</h3>
					</div><!-- /.listing-user-title -->
				</div>
				<div class="col-md-4 col-lg-3">
					<ul style="list-style-type:none; height:0px;">
					@if(! is_null($user->rate))
						<li>{{ $user->rate->rate->name }}</li>
						<li>
							@include('partials.app.stars.'. $user->rate->rate_id)
						</li>
					@endif
					</ul><!-- /.listing-row-social -->
				</div>
				<div>
					<p class="user-about">{{ $user->about }}</p>
				</div>
			</div>
		</div>

		@foreach($user->category->requirements as $requirement)
		@if(View::exists('app.user.categories_requirements.'. $requirement->id))
		@include('app.user.categories_requirements.'. $requirement->id)
		@endif
		@endforeach

		<div class="overview widget-background-white">
			<h2>{{ __('general.order_this_service')}} {{ $user->fullName }}</h2>
			<p>
				{{ __('general.interesed_in')}} {{ $user->fullName }} {{ __('general.services')}}
			</p>
			
			<form method="POST" action="{{ route('app.order.store') }}">
				@csrf
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>{{ __('general.want_to_say')}}</label>
							{!! Form::textarea('message', null, ['class' => 'form-control', 'rows' => 5, 'v-model' => 'message']) !!}
						</div><!-- /.form-group -->
					</div><!-- /.col-* -->

					<input type="hidden" name="beneficiary_id" value="{{ $user->id }}" />

					<div class="col-sm-12">
						<div class="form-group-btn">
							<button class="btn btn-primary pull-right">{{ __('general.send_order')}}</button>
						</div><!-- /.form-group-btn -->
					</div><!-- /.col-* -->					
				</div><!-- /.row -->
			</form>
		</div>

		@if(! is_null($similar_users)) 
		@include('app.user.show.similar_users', $similar_users)
		@endif
	</div>
	<!-- end user -->
</div>
@endsection