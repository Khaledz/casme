<div class="col-md-4 col-lg-3">
    <div class="widget listing-detail">
        <div class="overview widget-background-white">
            <h2>{{ __('general.personal_card')}}</h2>
            <ul>
                <li><strong>{{ __('general.name')}}</strong><span>{{ $user->fullName }}</span></li>
                <li><strong>{{ __('general.nationality')}}</strong><span>{{ $user->country->name }}</span></li>
                <li><strong>{{ __('general.residence')}}</strong><span>{{ $user->resident->name }}</span></li>			                
                <li><strong>{{ __('general.city')}}</strong><span>{{ $user->city }}</span></li>			                
                <li><strong>{{ __('general.gender')}}</strong><span>{{ $user->gender->name }}</span></li>
                @if(! is_null($user->height))
                <li><strong>{{ __('general.height')}}</strong><span>{{ $user->height }} {{ __('general.cm')}}</span></li>
                @endif
                @if(! is_null($user->weight))             
                <li><strong>{{ __('general.weight')}}</strong><span>{{ $user->weight }} {{ __('general.kg')}}</span></li>
                @endif
                @if(! is_null($user->age))		                
                <li><strong>{{ __('general.age')}}</strong><span>{{ $user->age }} {{ __('general.year')}}</span></li>
                @endif
                @if(! is_null($user->eye))		                
                <li><strong>{{ __('general.eye_color')}}</strong><span>{{ $user->eye->name }}</span></li>
                @endif
                @if(! is_null($user->skin))		                
                <li><strong>{{ __('general.skin_color')}}</strong><span>{{ $user->skin->name }}</span></li>
                @endif
                @if(! is_null($user->hair))		                
                <li><strong>{{ __('general.hair_color')}}</strong><span>{{ $user->hair->name }}</span></li>
                @endif           
                <li><strong>{{ __('general.can_travel')}}</strong><span>{{ $user->is_travel }}</span></li>                
            </ul>
        </div><!-- /.overview -->
    </div>			
</div><!-- /.col-* -->