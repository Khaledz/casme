@if(! is_null($user->price))
<div class="overview widget-background-white">
    <h2>{{ __('general.price')}}</h2>
    <ul class="amenities">
        <li class="yes">{{ $user->price->price}} {{ __('general.ryial')}}/{{ $user->price->type->name }}</li>
    </ul>
</div>
@endif