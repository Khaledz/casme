<div class="overview widget-background-white">
    <h2>{{ __('general.hand_pictures')}}</h2>
    @if(! is_null($user->getFrontHand()) && ! is_null($user->getBackHand()))
    <div class="row">
        <div class="col-md-6">
            <img src="{{ $user->getFrontHand() }}" width="350" height="300" />
            <p class="text-center">{{ __('general.front_hand')}}</p>
        </div>
        <div class="col-md-6">
            <img src="{{ $user->getBackHand() }}" width="350" height="300"/>
            <p class="text-center">{{ __('general.back_hand')}}</p>
        </div>
    </div>
    @else
    <p>{{ __('general.no_result')}}</p>
    @endif
</div>