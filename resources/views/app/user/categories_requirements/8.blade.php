<div class="overview widget-background-white">
    <h2>{{ __('general.accents')}}</h2>
    <ul class="amenities">
    @if($user->accents->count() == 0)
        <p>{{ __('general.no_result')}}</p>
    @else
        @foreach($user->accents as $accent)
        <li class="yes">{{ $accent->name }}</li>
        @endforeach
    @endif
    </ul>
</div>