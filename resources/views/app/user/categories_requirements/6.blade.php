<div class="overview widget-background-white">
    <h2>{{ __('general.introduction_video')}}</h2>
    @if(! is_null($user->video))
    <div class="embed-responsive embed-responsive-16by9">
    {!! $user->embed_video !!}
    </div>
    @else
    <p>{{ __('general.no_result')}}</p>
    @endif
</div>
