@if(! is_null($user->portfolios) && $user->portfolios->count() > 0)
<div class="overview widget-background-white">
    <h2>{{ __('general.protofolio')}}</h2>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>{{ __('general.protofolio_title')}}</th>
                <th>{{ __('general.protofolio_desc')}}</th>
                <th>{{ __('general.protofolio_url')}}</th>
            </tr>
        </thead>
        <tbody>
        @foreach($user->portfolios as $portofolio)
            <tr>
                <td>{{ $portofolio->id }}#</td>
                <td>{{ $portofolio->title }}</td>
                <td>{{ $portofolio->description }}</td>
                <td>
                    @if(is_null($portofolio->url))
                        -
                    @else
                        <a href="{{ $portofolio->url }}" target="_blank">{{ __('general.browse')}}</a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif