<h2>{{ __('general.similar_users')}} {{ $user->fullName }}</h2>
<div class="listing-box-wrapper">
    <div class="row">
        @if($similar_users->count() == 0)
            <p>{{ __('general.no_result')}}</p>
        @else
        @foreach($similar_users as $user)
        <div class="col-md-6 col-lg-4">
            <div class="listing-box">
                <div class="listing-box-image" style="background-image: url({{ $user->avatar() }})">
                    @if(! is_null($user->rate))
                    <div class="listing-box-image-label">
                        {{ $user->rate->rate->name }}
                    </div><!-- /.listing-box-image-label -->
                    @endif
                </div><!-- /.listing-box-image -->
                <div class="listing-box-title">
                    <span>{{ $user->fullName }}</span>
                    <!-- @if(! is_null($user->price))
                    <span class="pull-left">{{ $user->price->price}} ريال/{{ $user->price->type->name }}</span>
                    @endif -->
                    
                </div><!-- /.listing-box-title -->
                <div class="listing-box-content">
                   <dl>
                        <dt>{{ __('general.category') }}</dt><dd>{{ $user->category->name }}</dd>
                        <dt>{{ __('general.nationalty') }}</dt><dd>{{ $user->country->name }}</dd>
                        <dt>{{ __('general.residence') }}</dt><dd>{{ $user->country->name }}</dd>
                        <dt>{{ __('general.city') }}</dt><dd>{{ $user->city }}</dd>
                        <dt>{{ __('general.can_travel') }}</dt><dd>{{ $user->is_travel }}</dd>
                    </dl>
                    <a href="{{ route('app.user.show', $user)}}">
                        <button class="btn-primary btn-block btn-xs">{{ __('general.show_account') }}</button>
                    </a>
                </div><!-- /.listing-box-cotntent -->
            </div><!-- /.listing-box -->			
        </div><!-- /.col-* -->
        @endforeach
        @endif
    </div><!-- /.row -->	
</div><!-- /.listing-box-wrapper -->
