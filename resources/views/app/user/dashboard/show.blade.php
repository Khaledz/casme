@extends('layouts.app')
@section('title')
{{ __('general.control_panel')}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('partials.app.'. $user->roles()->first()->name .'_sidebar')
        </div>
        <div class="col-md-9">
            <!-- boxes -->
            @if($user->hasRole('beneficiary'))
            <div class="row">
                <div class="col-md-4">
                    <div class="dash-box dash-box-color-1">
                        <div class="dash-box-icon">
                            <i class="fa fa-money"></i>
                        </div>
                        <div class="dash-box-body">
                            <span class="dash-box-count">{{ $payment_count->balance }} {{ __('general.ryial')}}</span>
                            <span class="dash-box-title">{{ __('general.balance')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dash-box dash-box-color-2">
                        <div class="dash-box-icon">
                            <i class="fa fa-suitcase"></i>
                        </div>
                        <div class="dash-box-body">
                            <span class="dash-box-count">{{ $order_count }}</span>
                            <span class="dash-box-title">{{ __('general.order_count')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dash-box dash-box-color-3">
                        <div class="dash-box-icon">
                            <i class="fa fa-briefcase"></i>
                        </div>
                        <div class="dash-box-body">
                            <span class="dash-box-count">{{ $portofolio_count }}</span>
                            <span class="dash-box-title">{{ __('general.portofolio_count')}}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>{{ __('general.last_orders')}}</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('app.user.order') }}"><p>{{ __('general.show_all')}}</p></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">

                    @if($orders->count() == 0)
                        <p>{{ __('general.no_result')}}</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ __('general.order_status')}}</th>
                                <th>{{ __('general.price')}}</th>
                                <th>{{ __('general.show')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>#{{ $order->id }}</td>
                                <td>{{ $order->status()->first()->name }}</td>
                                <td>{{ is_null($order->price) ? __('general.under_pricing') : $order->price .__('general.ryial') }}</td>
                                <td>
                                    <a href="{{ route('app.order.show', $order) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-10">
                            <h4>{{ __('general.last_payment')}}</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('app.user.payment') }}"><p>{{ __('general.show_all')}}</p></a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if($payments->count() == 0)
                        <p>{{ __('general.no_result')}}</p>
                    @else
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>{{ __('general.payment_code')}}</th>
                                <th>{{ __('general.order')}}</th>
                                <th>{{ __('general.amount')}}</th>
                                <th>{{ __('general.status')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($payments as $payment)
                            <tr>
                                <td>#{{ $payment->id }}</td>
                                <td>{{ $payment->payment_code }}</td>
                                <td>{{ $payment->order->id }}</td>
                                <td>{{ $payment->amount }}</td>
                                <td>{{ $payment->status->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection