<?php 

return [
    'create' => 'Create',
    'edit' => 'Edit',
    'home_page' => 'Home Page',
    'user'=> 'User',
    'category' => 'Category',
    'order' => 'Order',
    'show' => 'Show',
	'login' => 'Login',
	'register' => 'Register',
	'service_menu' => 'Services List',
	'about_us' => 'About Us',
	'contact_us' => 'Contact Us',
	'copyright' => 'All copyrights reserved',
	'social_account' => 'Social Media Account',
	'terms' => 'Terms & Conditions',
	'privacy' => 'Privacy Policy',
	'professional_service' => 'Professional Service',
	'comprehensive' => 'Comprehensive Services',
	'understand_more' => 'We Understand More',
	'platforms' => 'All Platforms',
	'browse' => 'Browse',
	'professional_service_desc' => 'We offer you whatever you need to strat your project.',
	'comprehensive_desc' => 'We offer you an integrated service in all fields.',
	'understand_more_desc' => 'We understand you! We provide the search engine that you need.',
	'platforms_desc' => 'Our apps are available in all platforms.',
	'profile' => 'My Account',
	'control_panel' => 'Dashboard',
	'setting' => 'Settings',
	'logout' => 'Logout',
	'payment' => 'Payments',
	'order' => 'Orders',
	'protofolio'=> 'Portfolio',
	'myprofile' => 'Profile',
	'email' => 'Email',
	'password' => 'Password',
	'forget_password' => 'Forgot your password?',
	'setting' => 'Settings',
	'main_information' => 'Main Information',
	'fname' => 'First Name',
	'lname' => 'Last Name',
	'country' => 'Country',
	'password_conf' => 'Password Confirmation',
	'password_empty' => 'Leave it blank if you do not want to change',
	'avatar' => 'Avatar',
	'avatar_empty' => 'Leave it blank if you do not want to change',
	'save_setting'=> 'Save Settings',
	'twitter'=> 'Twitter',
	'facebook' => 'Facebook',
	'instagram' => 'Instagram',
	'snapchat' => 'Snapchat',
	'about' => 'About me',
	'city' => 'City',
	'phone' => 'Phone',
	'nationality' => 'Nationality',
	'residence' => 'Residence Country',
	'gender' => 'Gender',
	'category' => 'Category',
	'extra_information' => 'Extra Information',
	'weight' => 'Weight (KG)',
	'height' => 'Height (CM)',
	'age' => 'Age',
	'eye_color' => 'Eye Color',
	'skin_color' => 'Skin Color',
	'hair_color' => 'Hair Color',
	'upload back and front picture of hands.' => 'upload back and front picture of hands.',
	'back_hand' => 'back hand picture.',
	'front_hand' => 'front hand picture.',
	'price_range' => 'Price Range',
	'price_type' => 'Price Type',
	'price' => 'Price',
	'upload_video' => 'Upload a video introducing yourself.',
	'paste_video_url' => 'Paste url of your video from youtube.com',
	'accents_you_speak' => 'What accents are you speaking?',
	'can_travel' => 'Can Travel?',
	'yes' => 'Yes',
	'no' => 'No',
	'rate' => 'Rate',
	'height_from' => 'Height From',
	'to' => 'To',
	'weight_from' => 'Weight From',
	'age_from' => 'Age From',
	'price_from' =>'Price From',
	'notcare' =>'No Matter',
	'search' => 'Search',
	'order_this_service' => 'Order',
	'no_result' => 'There is no result at this moment.',
	'result_count' => 'Result Count',
	'filter_result' => 'Filter Result',
	'result' => 'Results',
	'interesed_in' => 'Are you intrested in ',
	'services' => 'services',
	'want_to_say' => 'Do you want to tell us more about your order? (optional)',
	'send_order' => 'Send Order',
	'hand_pictures' => 'Hands Pictrue',
	'ryial' => 'Ryial',
	'introduction_video' => 'Introduction Video',
	'protofolio_title' => 'Protofolio Title',
	'protofolio_desc' => 'Protofolio Description',
	'protofolio_url' => 'Protofolio URL',
	'accents' => 'Protofolio URL',
	'status' => 'Status',
	'amount' => 'Amount',
	'payment_code' => 'Payment Code',
	'show_all' => 'Show All',
	'last_payment' => 'Last Payments',
	'under_pricing' => 'Pricing ..',
	'order_status' => 'Order Status',
	'last_orders' => 'Last Orders',
	'portofolio_count' => 'Protofolio Count',
	'order_count' => 'Order Count',
	'balance' => 'Balance',
	'similar_users' => 'Similar users to',
	'personal_card' => 'Personal Card',
	'cm' => 'CM',
	'kg' => 'KG',
	'year' => 'Years',
	'show_account' => 'Show Account',
	'success' => 'Your data has been saved successfuly.'
];