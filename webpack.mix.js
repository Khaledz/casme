const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/app/js/app.js', 'public/js');
// .styles([
//     'resources/app/css/leaflet.css',
//     'resources/app/css/leaflet.markercluster.css',
//     'resources/app/css/leaflet.markercluster.default.css',
//     'resources/app/css/villareal-turquoise.css',
//     'resources/app/css/custom.rtl.css'
// ], 'public/css/app.css')
// .styles([
//     'resources/app/css/leaflet.css',
//     'resources/app/css/leaflet.markercluster.css',
//     'resources/app/css/leaflet.markercluster.default.css',
//     'resources/app/css/villareal-turquoise.css',
//     'resources/app/css/custom.rtl.css'
// ], 'public/css/app.css');
