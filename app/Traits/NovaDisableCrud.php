<?php 

namespace App\Traits;
Use Illuminate\Http\Request;

trait NovaDisableCrud
{
    public static function authorizedToCreate(Request $request)
    {
        return false;
    }
    
    public function authorizedToDelete(Request $request)
    {
        return false;
    }

    public function authorizedToUpdate(Request $request)
    {
        return false;
    }
}