<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Rate;
use App\Models\Order;
use App\Models\UserGender;
use App\Models\Country;
use App\Models\Category;
use App\Models\UserAccent;
use App\Models\SocialMedia;
use App\Models\PriceType;
use App\Models\UserEyeColor;
use App\Models\UserHairColor;
use App\Models\User;
use App\Models\Menu;
use App\Models\AppSetting;
use App\Models\UserSkinColor;
use App\Models\PaymentMethod;
use App\Models\PaymentStatus;
use App\Models\Payment;
use App\Observers\OrderObserv;
use App\Observers\PaymentObserv;
use App\Observers\CategoryObserv;
use App\Observers\UserObserv;
use App\Observers\RateObserv;
use App\Observers\AppSettingObserv;
use App\Observers\MenuObserv;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        // for nova translation package
        \Spatie\NovaTranslatable\Translatable::defaultLocales(['ar', 'en']);

        $countries = \Cache::rememberForever('countries', function () {
            return Country::orderBy('name', 'asc')->pluck('name', 'id');
        });

        $categories = \Cache::rememberForever('categories', function () {
            return Category::latest()->pluck('name', 'id');
        });

        $genders = \Cache::rememberForever('genders', function () {
            return UserGender::orderBy('id', 'asc')->pluck('name', 'id');
        });

        $rates = \Cache::rememberForever('rates', function () {
            return Rate::latest()->pluck('name', 'rate');
        });

        $accents = \Cache::rememberForever('accents', function () {
            return UserAccent::get();
        });

        $social_medias = \Cache::rememberForever('social_medias', function () {
            return SocialMedia::get();
        });

        $price_types = \Cache::rememberForever('price_types', function () {
            return PriceType::pluck('name', 'id');
        });

        $hair_colors = \Cache::rememberForever('hair_colors', function () {
            return UserHairColor::pluck('name', 'id');
        });

        $eye_colors = \Cache::rememberForever('eye_colors', function () {
            return UserEyeColor::pluck('name', 'id');
        });

        $skin_colors = \Cache::rememberForever('skin_colors', function () {
            return UserSkinColor::pluck('name', 'id');
        });

        $menu = \Cache::rememberForever('menu', function () {
            return Menu::get();
        });

        $payment_methods = \Cache::rememberForever('payment_methods', function () {
            return PaymentMethod::pluck('name', 'id');
        });

        $payment_statuses = \Cache::rememberForever('payment_statuses', function () {
            return PaymentStatus::pluck('name', 'id');
        });

        $app_setting = \Cache::rememberForever('app_setting', function () {
            return AppSetting::first();
        });

        \View::share('countries', $countries);
        \View::share('categories', $categories);
        \View::share('genders', $genders);
        \View::share('rates', $rates);
        \View::share('accents', $accents);
        \View::share('social_medias', $social_medias);
        \View::share('price_types', $price_types);
        \View::share('hair_colors', $hair_colors);
        \View::share('eye_colors', $eye_colors);
        \View::share('menu', $menu);
        \View::share('skin_colors', $skin_colors);
        \View::share('payment_methods', $payment_methods);
        \View::share('payment_statuses', $payment_statuses);
        \View::share('app_setting', $app_setting);
        
        Order::observe(OrderObserv::class);
        Payment::observe(PaymentObserv::class);
        Category::observe(CategoryObserv::class);
        User::observe(UserObserv::class);
        Rate::observe(RateObserv::class);
        AppSetting::observe(AppSettingObserv::class);
        Menu::observe(MenuObserv::class);




        // $countries = \Cache::rememberForever('countries', function () {
        //     $data = array();
        //     foreach(Country::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        //     // return Country::orderBy('name', 'asc')->pluck('name', 'id');
        // });

        // $categories = \Cache::rememberForever('categories', function () {
        //     $data = array();
        //     foreach(Category::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $genders = \Cache::rememberForever('genders', function () {
        //     $data = array();
        //     foreach(UserGender::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $rates = \Cache::rememberForever('rates', function () {
        //     $data = array();
        //     foreach(Rate::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $accents = \Cache::rememberForever('accents', function () {
        //     return UserAccent::get();
        // });

        // $social_medias = \Cache::rememberForever('social_medias', function () {
        //     return SocialMedia::get();
        // });

        // $price_types = \Cache::rememberForever('price_types', function () {
        //     $data = array();
        //     foreach(PriceType::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $hair_colors = \Cache::rememberForever('hair_colors', function () {
        //     $data = array();
        //     foreach(UserHairColor::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $eye_colors = \Cache::rememberForever('eye_colors', function () {
        //     $data = array();
        //     foreach(UserEyeColor::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $skin_colors = \Cache::rememberForever('skin_colors', function () {
        //     $data = array();
        //     foreach(UserSkinColor::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $menu = \Cache::rememberForever('menu', function () {
        //     return Menu::get();
        // });

        // $payment_methods = \Cache::rememberForever('payment_methods', function () {
        //     $data = array();
        //     foreach(PaymentMethod::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });

        // $payment_statuses = \Cache::rememberForever('payment_statuses', function () {
        //     $data = array();
        //     foreach(PaymentStatus::orderBy('name', 'asc')->get() as $object)
        //     {
        //         $data[$object->id] = $object->name;
        //     }
        //     return $data;
        // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
