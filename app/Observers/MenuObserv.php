<?php

namespace App\Observers;

use App\Models\Menu;
use Cache;

class MenuObserv
{
    /**
     * Handle the menu "created" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function created(Menu $menu)
    {
        Cache::forget('menu');
    }

    /**
     * Handle the menu "updated" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function updated(Menu $menu)
    {
        Cache::forget('menu');        
    }

    /**
     * Handle the menu "deleted" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function deleted(Menu $menu)
    {
        Cache::forget('menu');        
    }

    /**
     * Handle the menu "restored" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function restored(Menu $menu)
    {
        //
    }

    /**
     * Handle the menu "force deleted" event.
     *
     * @param  \App\Menu  $menu
     * @return void
     */
    public function forceDeleted(Menu $menu)
    {
        //
    }
}
