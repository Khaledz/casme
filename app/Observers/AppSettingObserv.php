<?php

namespace App\Observers;

use App\Models\AppSetting;
use Cache;

class AppSettingObserv
{
    /**
     * Handle the app_setting "created" event.
     *
     * @param  \App\AppSetting  $app_setting
     * @return void
     */
    public function created(AppSetting $app_setting)
    {
        Cache::forget('app_setting');
    }

    /**
     * Handle the app_setting "updated" event.
     *
     * @param  \App\AppSetting  $app_setting
     * @return void
     */
    public function updated(AppSetting $app_setting)
    {
        Cache::forget('app_setting');        
    }

    /**
     * Handle the app_setting "deleted" event.
     *
     * @param  \App\AppSetting  $app_setting
     * @return void
     */
    public function deleted(AppSetting $app_setting)
    {
        Cache::forget('app_setting');        
    }

    /**
     * Handle the app_setting "restored" event.
     *
     * @param  \App\AppSetting  $app_setting
     * @return void
     */
    public function restored(AppSetting $app_setting)
    {
        //
    }

    /**
     * Handle the app_setting "force deleted" event.
     *
     * @param  \App\AppSetting  $app_setting
     * @return void
     */
    public function forceDeleted(AppSetting $app_setting)
    {
        //
    }
}
