<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\User;
use App\Models\Balance;
use App\Notifications\NewOrder;
use App\Notifications\OrderPrice;
use Cache;

class OrderObserv
{
    /**
     * Handle the order "created" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function created(Order $order)
    {
        // notify admin & supervisor
        $users = User::role(['admin', 'supervisor'])->get();
        foreach($users as $user)
        {
            $user->notify(new NewOrder($order));
        }
    }

    /**
     * Handle the order "updated" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function updated(Order $order)
    {
        // we will notifiy the user if the order has changed from admin
        $order->user->notify(new OrderPrice($order));

        // if admin set price? then we notify the member
        if($order->status()->first()->id == 2)
        {
            $order->user->notify(new OrderPrice($order));
        }

        // and if order is completed? then send message to user.
        // also, add balance to service owner.
        if($order->status()->first()->id == 4)
        {
            // $order->user->notify(new OrderPrice($order));

            // also add balance to beneficiary
            $balance = \App\Models\Balance::where('user_id', $order->beneficiary->id)->first();
            $balance->balance += $order->price - $order->profit;
            $balance->save();
        }

        // if order is canceled? remove the money
        if($order->status()->first()->id == 5)
        {
            // remove balance from beneficiary
            $balance = \App\Models\Balance::where('user_id', $order->beneficiary->id)->first();
            $balance->balance -= $order->price - $order->profit;
            $balance->save();
            // $order->beneficiary->balance();
        }
        // dd('here');
    }

    /**
     * Handle the order "deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the order "restored" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the order "force deleted" event.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
