<?php

namespace App\Observers;

use App\Models\Payment;
use App\Models\User;
use App\Notifications\NewPayment;

class PaymentObserv
{
    /**
     * Handle the payment "created" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function created(Payment $payment)
    {
        // 1- update the order status to pending payment approval.
        $payment->order->update(['order_status_id' => 3]);

        // 2- notify admin & supervisor
        $users = User::role(['admin'])->get();
        foreach($users as $user)
        {
            $user->notify(new NewPayment($payment));
        }

        
    }

    /**
     * Handle the payment "updated" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function updated(Payment $payment)
    {
        // if payment changed to status 
        // $payment->user->notify(new PaymentPrice($payment));

        // 3- add the balance added to bene balance
        
    }

    /**
     * Handle the payment "deleted" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function deleted(Payment $payment)
    {
        //
    }

    /**
     * Handle the payment "restored" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function restored(Payment $payment)
    {
        //
    }

    /**
     * Handle the payment "force deleted" event.
     *
     * @param  \App\Payment  $payment
     * @return void
     */
    public function forceDeleted(Payment $payment)
    {
        //
    }
}
