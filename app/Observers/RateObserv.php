<?php

namespace App\Observers;

use App\Models\Rate;
use Cache;

class RateObserv
{
    /**
     * Handle the rate "created" event.
     *
     * @param  \App\Rate  $rate
     * @return void
     */
    public function created(Rate $rate)
    {
        Cache::forget('rates');
    }

    /**
     * Handle the rate "updated" event.
     *
     * @param  \App\Rate  $rate
     * @return void
     */
    public function updated(Rate $rate)
    {
        Cache::forget('rates');
    }

    /**
     * Handle the rate "deleted" event.
     *
     * @param  \App\Rate  $rate
     * @return void
     */
    public function deleted(Rate $rate)
    {
        Cache::forget('rates');
    }

    /**
     * Handle the rate "restored" event.
     *
     * @param  \App\Rate  $rate
     * @return void
     */
    public function restored(Rate $rate)
    {
        //
    }

    /**
     * Handle the rate "force deleted" event.
     *
     * @param  \App\Rate  $rate
     * @return void
     */
    public function forceDeleted(Rate $rate)
    {
        //
    }
}
