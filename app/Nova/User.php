<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Panel;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasOne;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\\Models\\User';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'fullName';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'fname', 'lname', 'email'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('Role', 'roles', 'App\Nova\Role')->sortable(),

            BelongsTo::make('Category', 'category', 'App\Nova\Category')->sortable()->rules('nullable')->nullable(),

            Text::make('First Name', 'fname')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Last Name', 'lname')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Password::make('Password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:6')
                ->updateRules('nullable', 'string', 'min:6'),

            Text::make('Phone')
                ->sortable()
                ->rules('required')->hideFromIndex(),

            Boolean::make('Status', 'status', $this)
                ->sortable()
                ->rules('required'),


            BelongsTo::make('Country')->hideFromIndex(),
            BelongsTo::make('Resident','resident', 'App\Nova\Country')->hideFromIndex(),
            BelongsTo::make('Gender', 'gender', 'App\Nova\UserGender')->hideFromIndex(),

            Text::make('City')
                ->sortable()
                ->rules('required')->hideFromIndex(),

            Avatar::make('Avatar', 'avatars'),

            
            new Panel('Extra Information', $this->beneficiaryFields()),

            MorphToMany::make('Roles', 'roles', \App\Nova\Role::class),
        ];
    }

    protected function beneficiaryFields()
    {
        return [
            Boolean::make('Can Travel', 'IsTravel', $this)->hideFromIndex(),
            Text::make('Height')->hideFromIndex(),
            Text::make('Weight')->hideFromIndex(),
            Text::make('Age')->hideFromIndex(),
            Text::make('Video', 'video', $this)->hideFromIndex(),
            Trix::make('About')->hideFromIndex(),
            BelongsTo::make('Eye Color', 'eye', 'App\Nova\UserEyeColor')
                ->sortable()
                ->rules('nullable')->hideFromIndex()->nullable(),

            BelongsTo::make('Skin Color', 'skin', 'App\Nova\UserSkinColor')
                ->sortable()
                ->rules('nullable')->hideFromIndex()->nullable(),

            BelongsTo::make('Hair Color', 'hair', 'App\Nova\UserHairColor')
                ->sortable()
                ->rules('nullable')->hideFromIndex()->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Users';
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'User';
    }
}
