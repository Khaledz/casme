<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\HasMany;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Spatie\NovaTranslatable\Translatable;
use Insenseanalytics\LaravelNovaPermission\PermissionsBasedAuthTrait;

class AppSetting extends Resource
{
    // use PermissionsBasedAuthTrait;

    // public static $permissionsForAbilities = [
    //     'viewAny' => 'appSetting-in-sidebar',
    //     'view' => 'appSetting-read',
    //     'create' => 'appSetting-create',
    //     'update' => 'appSetting-update',
    //     'delete' => 'appSetting-delete',
    // ];

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\AppSetting';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            ID::make()->sortable(),
            
            Translatable::make([
                Text::make('Application Title', 'website_title')->sortable()->rules('required'),
                Textarea::make('Application Description', 'about')->sortable()->rules('required'),
                Text::make('Copyright Text', 'copyright')->sortable()->rules('required'),
            ]),

            Text::make('Facbook Url', 'facebook')->sortable(),
            Text::make('Twitter Url', 'twitter')->sortable(),
            Text::make('Instagram Url', 'instagram')->sortable(),
            Text::make('Snapchat Url', 'snapchat')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function label()
    {
        return 'Applicaion Settings';
    }

    public static function authorizedToCreate(Request $request)
    {
        return false;
    }
    
    public function authorizedToDelete(Request $request)
    {
        return false;
    }
}
