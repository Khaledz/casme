<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Spatie\NovaTranslatable\Translatable;
use Insenseanalytics\LaravelNovaPermission\PermissionsBasedAuthTrait;

class Menu extends Resource
{
    use PermissionsBasedAuthTrait;

    public static $permissionsForAbilities = [
        'view' => 'menu-read',
        'viewAny' => 'menu-in-sidebar',
        'create' => 'menu-create',
        'update' => 'menu-update',
        'delete' => 'menu-delete',
        'addCategory' => 'category-create',
        'addMenu' => 'menu-create',
        'addAnyMenu' => 'menu-create',

    ];
    
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Menu';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Translatable::make([
                Text::make('Menu Name', 'name')->sortable()->rules('required')
            ]),
            BelongsTo::make('Associated Category', 'category', 'App\Nova\Category')->rules('required'),
            HasMany::make('Sub menu', 'children', 'App\Nova\Menu')->rules('required', 'uniqe: menus,id'),
            BelongsTo::make('Parent Menu', 'parent', 'App\Nova\Menu')->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
