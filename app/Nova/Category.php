<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasOne;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Spatie\NovaTranslatable\Translatable;
use Laravel\Nova\Fields\Boolean;
use Insenseanalytics\LaravelNovaPermission\PermissionsBasedAuthTrait;

class Category extends Resource
{
    // use PermissionsBasedAuthTrait;

    // public static $permissionsForAbilities = [
    //     'view' => 'category-read',
    //     'viewAny' => 'category-in-sidebar',
    //     'create' => 'category-create',
    //     'update' => 'category-update',
    //     'delete' => 'category-delete',
    //     'addAttachment' => 'attachment-create',
    //     'addMenu' => 'menu-create',
    //     'attachCategoryRequirement' => 'categoryRequirement-create',
    //     'attachAnyCategoryRequirement' => 'categoryRequirement-create',
    // ];

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Category';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('Type','type', 'App\Nova\CategoryType'),
            HasOne::make('Background Image','attachment', 'App\Nova\Attachment'),

            BelongsToMany::make('Requirements', 'requirements', 'App\Nova\CategoryRequirement', 'category_requirement_id')->rules('required', 'exists:categories_requirements,category_requirement_id'),
            Translatable::make([
                Text::make('name')->sortable()->rules('required'),
            ]),
            Boolean::make('Display this category in home?', 'in_home')
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
