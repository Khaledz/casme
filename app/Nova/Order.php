<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\BelongsTo;
use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;
use Insenseanalytics\LaravelNovaPermission\PermissionsBasedAuthTrait;

class Order extends Resource
{
    use PermissionsBasedAuthTrait;

    public static $permissionsForAbilities = [
        'viewAny' => 'order-in-sidebar',
        'view' => 'order-read',
        'create' => 'order-create',
        'update' => 'order-update',
        'delete' => 'order-delete',
    ];

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Models\Order';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'Order#'.'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'price', 'margin_profit'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            BelongsTo::make('User', 'user', 'App\Nova\User'),
            BelongsTo::make('Status', 'status', 'App\Nova\OrderStatus'),
            BelongsTo::make('Beneficiary', 'beneficiary', 'App\Nova\User'),
            Text::make('Price')->sortable()->rules('int'),
            Trix::make('Message')->sortable(),
            // Text::make('Margin Profit(%)', 'profit_margin')->sortable(),
            Text::make('Profit')->sortable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
