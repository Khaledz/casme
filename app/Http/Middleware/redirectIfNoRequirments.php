<?php

namespace App\Http\Middleware;

use Closure;

class redirectIfNoRequirments
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
            if there is auth, and the role is bene, and basic data is not available?
            also, I need to fetch all requirment to see if this user is meet the requirment or not
            fname,lname, country, resident, city, 
        */
        
        if(auth()->check() && ! $request->is('user/settings') && ! auth()->user()->hasRole('admin'))
        {
            if(is_null(auth()->user()->fname)
            || is_null(auth()->user()->lname)
            || is_null(auth()->user()->country_id)
            || is_null(auth()->user()->residence_country_id)
            || is_null(auth()->user()->city)
            || is_null(auth()->user()->user_gender_id)
            || is_null(auth()->user()->password)
            || is_null(auth()->user()->email))
            {
                return redirect(route('app.user.setting'))->with('message', 'عزيزي ' . auth()->user()->fullName .' لخدمتك بشكل أفضل يجب عليك تعبئة كامل الحقول.');
            }

            if(auth()->user()->hasRole('beneficiary'))
            {
                if(is_null(auth()->user()->user_category_id))
                {
                    return redirect(route('app.user.setting'))->with('message', 'عزيزي ' . auth()->user()->fullName .' لخدمتك بشكل أفضل يجب عليك تعبئة كامل الحقول.');
                }
            }
        }
        return $next($request);
    }
}
