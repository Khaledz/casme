<?php

namespace App\Http\Controllers\Api;

use App\Models\Payment as PaymentModel;
use App\Models\Order;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\ResultPrinter;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();

        return view('app.portofolio.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::findOrFail($request->order_id);

        $clientId = env('paypalclient_id');
        $clientSecret = env('paypalclient_secret');

        $apiContext = $this->getApiContext($clientId, $clientSecret);

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName($order->beneficiary->fullName)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($order->price / 3.75);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $amount = new Amount();
        $amount->setCurrency("USD")
        ->setTotal($order->price / 3.75);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Order a service for " . $order->beneficiary->fullName)
        ->setInvoiceNumber(uniqid());

        $baseUrl = config('app.url');
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("$baseUrl")
            ->setCancelUrl("$baseUrl");

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $request = clone $payment;

        try {
    $response = $payment->create($apiContext);
	
} catch (\PayPal\Exception\PPConnectionException $pce) {
    return json()->response(['data' => 'something went wrong'], 422);
}

        

        // session()->put('apiContext', $apiContext);

        // $approvalUrl = $payment->getApprovalLink();
		return response()->json([
            'data' => $payment->getApprovalLink()
        ]);

        // return redirect($approvalUrl);    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function show(Balance $balance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function edit(Balance $balance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Balance $balance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Balance $balance)
    {
        //
    }

    /**
     * Helper method for getting an APIContext for all calls
     * @param string $clientId Client ID
     * @param string $clientSecret Client Secret
     * @return PayPal\Rest\ApiContext
     */
    function getApiContext($clientId, $clientSecret)
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        $apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'log.LogEnabled' => true,
                'log.FileName' => '../PayPal.log',
                'log.LogLevel' => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => true,
            )
        );

        return $apiContext;
    }

    public function successful(Request $request)
    {
		// ?paymentId=PAYID-LTG2NIQ1CV27510F2183383M&token=EC-78G20285FD215611Y&PayerID=VGRWDMK6DMVU8
		$clientId = env('paypalclient_id');
        $clientSecret = env('paypalclient_secret');
        $order = \App\Models\Order::findOrFail($request->order_id);
        $apiContext = $this->getApiContext($clientId, $clientSecret);

        $payment = new PaymentModel();
        $payment->payment_code = $request->paymentId;
        $payment->user_id = auth()->user()->id;
        $payment->payment_status_id = 1;
        $payment->payment_method_id = 2;
        $payment->order_id = $order->id;
        $payment->amount = $order->price;
        $payment->save();

        $paymentId = $request->paymentId;
		$payment = Payment::get($paymentId, $apiContext);
		return response()->json(['data' => 'success', 'payment' => $payment]);
    }
}

