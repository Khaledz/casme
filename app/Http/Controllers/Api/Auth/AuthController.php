<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Models\SocialMedia;
use App\Models\UserPortfolio;
use App\Models\Category;
use App\Models\Balance;
use MediaEmbed\MediaEmbed;
use Socialite;

class AuthController extends Controller
{
	use RegistersUsers;

    public function login(Request $request)
	{
		$request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return response()->json([
                'status' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
	}

	/**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create_beneficiary(array $data)
    {
        $user = User::create([
            'user_category_id' => $data['user_category_id'],
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'about' => $data['about'],
            'city' => $data['city'],
            'country_id' => $data['country_id'],
            'user_gender_id' => $data['user_gender_id'],
            'is_travel' => $data['is_travel'],
            'password' => Hash::make($data['password']),
            'video' => $data['video'],
            'height' => $data['height'],
            'age' => $data['age'],
            'weight' => $data['weight'],
            'eye_color_id' => $data['eye_color_id'],
            'hair_color_id' => $data['hair_color_id'],
            'skin_color_id' => $data['skin_color_id'],
            'residence_country_id' => $data['residence_country_id']
        ]);

		// if there is introduction video, we need to conver it to embed url.
        if(isset($data['video']) && ! is_null($data['video']))
        {
            $MediaEmbed = new MediaEmbed();
            $data['embed_video'] = $MediaEmbed->parseUrl($data['video'])->setAttribute([
                'width' => '100%',
            ])->getEmbedCode();
            $user->update(['video' => $data['video'], 'embed_video' => $data['embed_video']]);
        }

        $this->storePrice($data, $user);
        $this->storeSocialMedia($data, $user);
        $this->storeAccents($data, $user);
        $this->storePortfolio($data, $user);
        $this->handleUploads($data, $user);

        return $user;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create_member(array $data)
    {
        $user = User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'about' => $data['about'],
            'city' => $data['city'],
            'country_id' => $data['country_id'],
            'user_gender_id' => $data['user_gender_id'],
            'password' => Hash::make($data['password']),
            'residence_country_id' => $data['residence_country_id']
        ]);

        $this->handleUploads($data, $user);

        return $user;
    }

	public function register_member(Request $request)
	{
		$request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'country_id' => 'required|integer|exists:countries,id',
            'city' => 'required|min:3',
            'user_gender_id' => 'required|integer|exists:user_genders,id',
            'about' => 'required|max:4000|min:10',
            'agreement' => 'accepted',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'residence_country_id' => 'required|integer|exists:countries,id'
        ]);

        event(new Registered($user = $this->create_member($request->all())));

        $user->assignRole('member');

        $this->guard()->login($user);

		$tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
			'message' => 'Successfully created user!'
        ], 201);
	}

	public function register_beneficiary(Request $request)
    {
        $validations = [
            'user_category_id' => 'required|integer|exists:categories,id',
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'country_id' => 'required|integer|exists:countries,id',
            'user_gender_id' => 'required|integer|exists:user_genders,id',
            'is_travel' => 'nullable|integer',
            'city' => 'required|max:255',
            'about' => 'required|max:4000|min:10',
            'facebook' => 'nullable|max:4000|min:3',
            'twitter' => 'nullable|max:4000|min:3',
            'instagram' => 'nullable|max:4000|min:3',
            'snapchat' => 'nullable|max:4000|min:3',
            'agreement' => 'accepted',
            'residence_country_id' => 'required|integer|exists:countries,id'
        ];

        if(! is_null($request->user_category_id))
        {
            $requirements = Category::find($request->user_category_id)->requirements;

            if($requirements->count() > 0)
            {
                foreach($requirements as $requirement)
                {
                    $validations = array_merge($validations, $requirement->validations);
                }
            }
        }

        $this->validate($request, $validations);

        event(new Registered($user = $this->create_beneficiary($request->all())));

        $user->assignRole('beneficiary');

        $balance = new Balance();
        $balance->user_id = $user->id;
        $balance->save();

        $this->guard()->login($user);

		$tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
			'message' => 'Successfully created user!'
        ], 201);

        // return $this->registered($request, $user);
    }

	private function storeSocialMedia($data, $user)
    {
        if(isset($data['facebook']) && ! is_null($data['facebook']))
        {
            $facebook = SocialMedia::where('english_name', 'facebook')->first();
            $user->socials()->attach($facebook, ['url' => $data['facebook']]);
        }

        if(isset($data['instagram']) && ! is_null($data['instagram']))
        {
            $instagram = SocialMedia::where('english_name', 'instagram')->first();
            $user->socials()->attach($instagram, ['url' => $data['instagram']]);
        }

        if(isset($data['twitter']) && ! is_null($data['twitter']))
        {
            $twitter = SocialMedia::where('english_name', 'twitter')->first();
            $user->socials()->attach($twitter, ['url' => $data['twitter']]);
        }

        if(isset($data['snapchat']) && ! is_null($data['snapchat']))
        {
            $snapchat = SocialMedia::where('english_name', 'snapchat')->first();
            $user->socials()->attach($snapchat, ['url' => $data['snapchat']]);
        }
    }

    private function storePrice($data, $user)
    {
        if(isset($data['price_type_id']) && !is_null($data['price_type_id'])
		&& isset($data['price']) && !is_null($data['price']))
        {
            \App\Models\UserPrice::create([
                'price_type_id' => $data['price_type_id'],
                'price' => $data['price'],
                'user_id' => $user->id
            ]);
        }
    }

    private function storeAccents($data, $user)
    {
        if(isset($data['accents']) && ! is_null($data['accents']))
        {
            $user->accents()->attach(explode(',', $data['accents']));
        }
    }

    private function storePortfolio($data, $user)
    {
        if(isset($data['portfolio_title']) && ! is_null($data['portfolio_title'])
		&& isset($data['portfolio_url']) && ! is_null($data['portfolio_url']))
        {
            $portfolio = new UserPortfolio();
            $portfolio->title = $data['portfolio_title'];
            $portfolio->url = $data['portfolio_url'];
            $portfolio->user_id = $user->id;
            $portfolio->save();
            $user->portfolios()->save($portfolio);
        }
    }

    public function handleUploads($data, $user)
    {
        if(request()->hasFile('back'))
        {
            request()->file('back')->store('public');
            $file = request()->file('back')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('back')->getClientOriginalName(),
                    'description' => null,
                    'type_id' => 3,
                    'model' => 'App\Models\User',
                    'user_id' => $user->id,
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }

        if(request()->hasFile('front'))
        {
            request()->file('front')->store('public');
            $file = request()->file('front')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('front')->getClientOriginalName(),
                    'description' => null,
                    'user_id' => $user->id,
                    'type_id' => 2,
                    'model' => 'App\Models\User',
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }

        if(request()->hasFile('avatar'))
        {
            request()->file('avatar')->store('public');
            $file = request()->file('avatar')->hashName();
            $user->update(['avatar' => $file]);
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('avatar')->getClientOriginalName(),
                    'description' => null,
                    'user_id' => $user->id,
                    'type_id' => 6,
                    'model' => 'App\Models\User',
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }
    }

    private function convertYoutubeToEmbed($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id;
    }

    private function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

	/**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

	/**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
}
