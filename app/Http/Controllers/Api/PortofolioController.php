<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserPortfolio;

class PortofolioController extends Controller
{
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:3|max:1000',
            'description' => 'nullable|min:3|max:5000',
            'url' => 'nullable|min:3|max:191|url',
            'attachment' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if(request()->hasFile('attachment'))
        {
            request()->file('attachment')->store('public');
            $file = request()->file('attachment')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('attachment')->getClientOriginalName(),
                    'description' => $request->description,
                    'user_id' => $request->user('api')->id,
                    'type_id' => 5,
                    'model' => 'App\Models\UserPortfolio',
                    'model_id' => $request->user('api')->id
                ]
            );

            auth()->user()->attachments()->save($attachment);
        }

        $portfolio = UserPortfolio::create(array_merge($request->except('attachment'), ['user_id' => auth()->user()->id]));

		return response()->json([
            'data' => 'Portofolio Created Successfully.'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function update(UserPortfolio $portfolio, Request $request)
    // {
    //     return $request->all();
    //     $this->validate($request, [
    //         'title' => 'required|min:3|max:1000',
    //         'description' => 'nullable|min:3|max:5000',
    //         'url' => 'nullable|min:3|max:191|url',
    //         'attachment' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048'
    //     ]);

    //     if(request()->hasFile('attachment'))
    //     {
    //         request()->file('attachment')->store('public');
    //         $file = request()->file('attachment')->hashName();
    //         $attachment = \App\Models\Attachment::create(
    //             [
    //                 'name' => $file,
    //                 'orginal_name' => request()->file('attachment')->getClientOriginalName(),
    //                 'description' => $request->description,
    //                 'user_id' => $request->user('api')->id,
    //                 'type_id' => 5,
    //                 'model' => 'App\Models\UserPortfolio',
    //                 'model_id' => $request->user('api')->id
    //             ]
    //         );

    //         auth()->user()->attachments()->save($attachment);
    //     }

    //     $portfolio = UserPortfolio::update(array_merge($request->except('attachment'), ['user_id' => auth()->user()->id]));

	// 	return response()->json([
    //         'data' => 'Portofolio Created Successfully.'
    //     ]);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPortfolio $portfolio, Request $request)
    {
        UserPortfolio::findOrFail($request->id)->delete();

        return response()->json([
            'data' => 'Portofolio Deleted Successfully.'
        ]);
    }
}