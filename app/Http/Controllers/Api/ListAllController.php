<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class ListAllController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function countries()
    {
        $countries = \Cache::rememberForever('countries', function () {
            return Country::orderBy('name', 'asc')->pluck('name', 'id');
        });

		return response()->json([
            'data' => $countries
        ]);
    }

	public function categories()
    {
        $categories = Category::get();

		return response()->json([
            'data' => $categories
        ]);
    }

	public function genders()
    {
        $genders = \App\Models\UserGender::orderBy('id', 'asc')->get();

		return response()->json([
            'data' => $genders
        ]);
    }

	public function rates()
    {
        $rates = \App\Models\Rate::latest()->get();

		return response()->json([
            'data' => $rates
        ]);
    }

	public function accents()
    {
        $accents = \App\Models\UserAccent::get();

		return response()->json([
            'data' => $accents
        ]);
    }

	public function social_medias()
    {
        $social_medias = \Cache::rememberForever('social_medias', function () {
            return SocialMedia::get();
        });

		return response()->json([
            'data' => $social_medias
        ]);
    }

	public function price_types()
    {
        $price_types = \App\Models\PriceType::get();

		return response()->json([
            'data' => $price_types
        ]);
    }
	
	public function hair_colors()
    {
        $hair_colors = \App\Models\UserHairColor::get();

		return response()->json([
            'data' => $hair_colors
        ]);
    }

	public function eye_colors()
    {
        $eye_colors = \App\Models\UserEyeColor::get();

		return response()->json([
            'data' => $eye_colors
        ]);
    }

	public function skin_colors()
    {
        $skin_colors = \App\Models\UserSkinColor::get();

		return response()->json([
            'data' => $skin_colors
        ]);
    }

	public function menu()
    {
        $menu = \Cache::rememberForever('menu', function () {
            return Menu::get();
        });

		return response()->json([
            'data' => $menu
        ]);
    }

	public function payment_methods()
    {
        $payment_methods = \App\Models\PaymentMethod::get();

		return response()->json([
            'data' => $payment_methods
        ]);
    }

	public function payment_statuses()
    {
        $payment_statuses = \App\Models\PaymentStatus::get();

		return response()->json([
            'data' => $payment_statuses
        ]);
    }

	public function app_setting()
    {
        $app_setting = \Cache::rememberForever('app_setting', function () {
            return AppSetting::first();
        });

		return response()->json([
            'data' => $app_setting
        ]);
    }

	public function roles()
    {
        $roles = \Spatie\Permission\Models\Role::get();

		return response()->json([
            'data' => $roles
        ]);
    }
}