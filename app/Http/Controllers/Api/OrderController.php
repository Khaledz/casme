<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;

class OrderController extends Controller
{
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['message' => 'nullable|max:5000|min:3']);

        $order = new Order();
        $order->order_status_id = 1;
        $order->user_id = $request->user('api')->id;
        $order->beneficiary_id = $request->beneficiary_id;
        $order->message = $request->message;
        $order->save();

		return response()->json([
            'data' => 'Order Created Successfully.'
        ]);
    }

	/**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order, Request $request)
    {
        if($order->load(['user', 'beneficiary', 'status'])->where('user_id', $request->user('api')->id)->orWhere('beneficiary_id', $request->user('api')->id)->count() > 0)
        {
            $user = $request->user('api');
            if(! is_null($order->price) && $order->status()->first()->id == 2)
            {
                $is_payment = true;
            }
            else
            {
                $is_payment = false;
            }

			return response()->json([
				'data' => [
                    'order' => $order,
                    'is_payment' => $is_payment
				]
			]);
        }

        return response()->json([], 403);
    }
}