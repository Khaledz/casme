<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Order;
use App\Models\UserPortfolio;
use App\Models\Payment;
use App\Models\Balance;
use App\Models\Category;
use App\Models\SocialMedia;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use MediaEmbed\MediaEmbed;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = User::find($request->user_id);
        if($user->hasRole('beneficiary'))
        {
            $similar_users = User::where('id', '!=', $user->id)
            ->where('user_category_id', $user->category->id)
            ->inRandomOrder()
            ->limit(3)
            ->get();

			return response()->json([
                'user' => $user,
				'similar_users' => $similar_users
			]);
        }

		return response()->json([
            'user' => $user,
			'similar_users' => []
		]);
    }

    public function dashboard(Request $request)
    {
        $user = $request->user('api');

        $orders = Order::where('user_id', $user->id)->latest()->limit(5)->get();
        $payments = Payment::where('user_id', $user->id)->latest()->limit(5)->get();

        $order_count = Order::where('beneficiary_id', $user->id)->count();
        if(Balance::where('user_id', $user->id)->first() != null)
        {
            $payment_count = Balance::where('user_id', $user->id)->first()->balance;
        }
        else
        {
            $payment_count = null;
        }


        $portofolio_count = UserPortfolio::where('user_id', $user->id)->count();

		return response()->json([
			'orders' => $orders,
			'payments' => $payments,
			'order_count' => $order_count,
			'payment_count' => $payment_count,
			'portofolio_count' => $portofolio_count
		]);
    }

    public function post_setting(Request $request)
    {
        $data = $request->toArray();
        $user = $request->user('api');

        $validations = [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'. $user->id],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'country_id' => 'required|integer|exists:countries,id',
            'user_gender_id' => 'required|integer|exists:user_genders,id',
            'is_travel' => 'nullable|integer',
            'city' => 'required|max:255',
            'about' => 'required|max:4000|min:10',
            'facebook' => 'nullable|max:4000|min:3',
            'twitter' => 'nullable|max:4000|min:3',
            'instagram' => 'nullable|max:4000|min:3',
            'snapchat' => 'nullable|max:4000|min:3',
            'residence_country_id' => 'required|integer|exists:countries,id'
        ];

        if(! is_null($user->user_category_id))
        {
            $requirements = Category::find($user->user_category_id)->requirements;

            if($requirements->count() > 0)
            {
                foreach($requirements as $requirement)
                {
                    $validations = array_merge($validations, $requirement->validations);
                }
            }
        }

        // if register with social, there is no password so it will require it.
        if(is_null($user->password))
        {
            $validations = array_merge($validations, ['password' => ['required', 'string', 'min:6', 'confirmed']]);
        }

        if(is_null($user->user_category_id))
        {
            $validations = array_merge($validations, ['user_category_id' => 'required|integer|exists:categories,id']);
        }

        $this->validate($request, $validations);

        if(isset($data['password']) && ! is_null($data['password']))
        {
            // $data['password'] = Hash::make($data['password']);
            $user->update(['password' => Hash::make($data['password'])]);
        }

        // if user is bene
        if($user->hasRole('beneficiary'))
        {
            // if there is introduction video, we need to conver it to embed url.
            if(isset($data['video']) && ! is_null($data['video']))
            {
                $MediaEmbed = new MediaEmbed();
                if(is_null($MediaEmbed->parseUrl($data['video'])))
                {
                    return back()->with('message', 'يبدو أن رابط الفيديو غير صحيح، من فضلك تأكد أن الرابط يحتوي على فيديو.');
                }
                $data['embed_video'] = $MediaEmbed->parseUrl($data['video'])->getEmbedCode();
                $user->update(['video' => $data['video'], 'embed_video' => $data['embed_video']]);
            }

            if(isset($data['user_category_id']) && ! is_null($data['user_category_id']))
            {
                $user->update(['user_category_id' => $data['user_category_id']]);
            }

            if(isset($data['height']) && ! is_null($data['height']))
            {
                $user->update(['height' => $data['height'], 'weight' => $data['weight'], 'age' => $data['age']]);
            }

            if(isset($data['eye_color_id']) && ! is_null($data['eye_color_id']))
            {
                $user->update(['eye_color_id' => $data['eye_color_id']]);
            }

            if(isset($data['hair_color_id']) && ! is_null($data['hair_color_id']))
            {
                $user->update(['hair_color_id' => $data['hair_color_id']]);
            }

            if(isset($data['skin_color_id']) && ! is_null($data['skin_color_id']))
            {
                $user->update(['skin_color_id' => $data['skin_color_id']]);
            }

            if(isset($data['is_travel']) && ! is_null($data['is_travel']))
            {
                $user->update(['is_travel' => $data['is_travel']]);
            }

            $user->update([
                // 'user_category_id' => $data['user_category_id'],
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'about' => $data['about'],
                'city' => $data['city'],
                'country_id' => $data['country_id'],
                'user_gender_id' => $data['user_gender_id'],
                'residence_country_id' => $data['residence_country_id'],
            ]);

            $this->storePrice($data, $user);
            $this->storeSocialMedia($data, $user);
            $this->storeAccents($data, $user);
        }
        else if($user->hasRole('member'))
        {
            $user->update([
                // 'user_category_id' => $data['user_category_id'],
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'about' => $data['about'],
                'city' => $data['city'],
                'country_id' => $data['country_id'],
                'user_gender_id' => $data['user_gender_id'],
                'residence_country_id' => $data['residence_country_id'],
            ]);
        }

        $this->handleUploads($data, $user);

		return response()->json([
			'message' => 'User settings updated successfully.',
		]);
    }

    public function payments(Request $request)
    {
        $user = $request->user('api');
        $payments = Payment::where('user_id', $user->id)->paginate(10);

		return response()->json([
			'payments' => $payments,
		]);
    }

    public function portofolios(Request $request)
    {
        $user = $request->user('api');
        $portofolios = UserPortfolio::where('user_id', $user->id)->paginate(10);

		return response()->json([
			'portofolios' => $portofolios,
		]);
    }

    public function orders(Request $request)
    {
        $user = $request->user('api');
        $orders = Order::where('user_id', $user->id)->orWhere('beneficiary_id', $user->id)->paginate(10);

        return response()->json([
			'orders' => $orders,
		]);
    }

    private function storeSocialMedia($data, $user)
    {
        if(isset($data['facebook']) && ! is_null($data['facebook']))
        {
            $facebook = SocialMedia::where('english_name', 'facebook')->first();
            $user->socials()->sync([$facebook->id => ['url' => $data['facebook']]], false);
        }
        else
        {
            $facebook = SocialMedia::where('english_name', 'facebook')->first();
            $user->socials()->detach($facebook);
        }

        if(isset($data['instagram']) && ! is_null($data['instagram']))
        {
            $instagram = SocialMedia::where('english_name', 'instagram')->first();
            $user->socials()->sync([$instagram->id => ['url' => $data['instagram']]], false);
        }
        else
        {
            $instagram = SocialMedia::where('english_name', 'instagram')->first();
            $user->socials()->detach($instagram);
        }

        if(isset($data['twitter']) && ! is_null($data['twitter']))
        {
            $twitter = SocialMedia::where('english_name', 'twitter')->first();
            $user->socials()->sync([$twitter->id => ['url' => $data['twitter']]], false);
        }
        else
        {
            $twitter = SocialMedia::where('english_name', 'twitter')->first();
            $user->socials()->detach($twitter);
        }

        if(isset($data['snapchat']) && ! is_null($data['snapchat']))
        {
            $snapchat = SocialMedia::where('english_name', 'snapchat')->first();
            $user->socials()->sync([$snapchat->id => ['url' => $data['snapchat']]], false);
        }
        else
        {
            $snapchat = SocialMedia::where('english_name', 'snapchat')->first();
            $user->socials()->detach($snapchat);
        }
    }

    private function storePrice($data, $user)
    {
        if(isset($data['price_type_id']) && ! is_null($data['price_type_id']))
        {
            $user->price()->update([
                'price_type_id' => $data['price_type_id'],
                'price' => $data['price'],
                'user_id' => $user->id
            ]);
        }
    }

    private function storeAccents($data, $user)
    {
        if(isset($data['accents']) && ! is_null($data['accents']))
        {
            $user->accents()->sync($data['accents']);
        }
        else
        {
            $user->accents()->detach();
        }
    }

    private function storePortfolio($data, $user)
    {
        if(! is_null($data['portfolio_title']) && ! is_null($data['portfolio_url']))
        {
            $portfolio = new UserPortfolio();
            $portfolio->title = $data['portfolio_title'];
            $portfolio->url = $data['portfolio_url'];
            $portfolio->user_id = $user->id;
            $portfolio->save();
            $user->portfolios()->save($portfolio);
        }
    }

    public function handleUploads($data, $user)
    {
        if(request()->hasFile('back'))
        {
            request()->file('back')->store('public');
            $file = request()->file('back')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('back')->getClientOriginalName(),
                    'description' => null,
                    'type_id' => 3,
                    'model' => 'App\Models\User',
                    'user_id' => $user->id,
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }

        if(request()->hasFile('front'))
        {
            request()->file('front')->store('public');
            $file = request()->file('front')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('front')->getClientOriginalName(),
                    'description' => null,
                    'user_id' => $user->id,
                    'type_id' => 2,
                    'model' => 'App\Models\User',
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }

        if(request()->hasFile('avatar'))
        {
            request()->file('avatar')->store('public');
            $file = request()->file('avatar')->hashName();
            $user->update(['avatar' => $file]);
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('avatar')->getClientOriginalName(),
                    'description' => null,
                    'user_id' => $user->id,
                    'type_id' => 6,
                    'model' => 'App\Models\User',
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }
    }

    private function convertYoutubeToEmbed($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id;
    }

    private function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

    public function set_category_post(Request $request)
    {
        $this->validate($request, ['category_id' => 'required|integer|exists:categories,id']);

        $request->user('api')->update(['user_category_id' => $request->category_id]);

		return response()->json([
			'message' => 'User category updated successfully.',
		]);
    }

}
