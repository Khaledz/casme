<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::getHomeCategories();

        return view('app.home.index', compact('categories'));
    }

    public function contact()
    {
        return view('app.home.contact');
    }

    public function about()
    {
        return view('app.home.about');
    }

    public function terms()
    {
        return view('app.home.terms');
    }

    public function privacy()
    {
        return view('app.home.privacy');
    }
}
