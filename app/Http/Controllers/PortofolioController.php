<?php

namespace App\Http\Controllers;

use App\Models\UserPortfolio;
use Illuminate\Http\Request;

class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();

        return view('app.portofolio.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:3|max:1000',
            'description' => 'nullable|min:3|max:5000',
            'url' => 'nullable|min:3|max:191|url',
            'attachment' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if(request()->hasFile('attachment'))
        {
            request()->file('attachment')->store('public');
            $file = request()->file('attachment')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('attachment')->getClientOriginalName(),
                    'description' => $request->description,
                    'user_id' => auth()->user()->id,
                    'type_id' => 5,
                    'model' => 'App\Models\UserPortfolio',
                    'model_id' => auth()->user()->id
                ]
            );

            auth()->user()->attachments()->save($attachment);
        }

        $portfolio = UserPortfolio::create(array_merge($request->except('attachment'), ['user_id' => auth()->user()->id]));

        return redirect(route('app.user.portofolio'))->with('message', __('portofolio.success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function show(Balance $balance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function edit(Balance $balance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Balance $balance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPortfolio $portfolio)
    {
        return $portfolio->delete();
    }
}
