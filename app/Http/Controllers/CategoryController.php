<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();

        return view('app.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $users = $category->users()->paginate(12);
        $model = collect();

        return view('app.category.show', compact('category', 'users', 'model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    /**
     * Get the requirment of category.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function getRequirements(Request $request)
    {
        $category = Category::find($request->category_id);
        $requirements = $category->requirements;

        return response()->json($requirements);
    }

    public function filter(Request $request)
    {
        $result = \App\Models\User::role('beneficiary');

        if(! is_null($request->category_id))
        {
            $result->where('user_category_id', $request->category_id);
        }

        if(! is_null($request->country_id))
        {
            $result->where('country_id', $request->country_id);
        }

        if(! is_null($request->city))
        {
            $result->where('city', 'like', '%' . $request->city . '%');
        }

        if(! is_null($request->gender_id))
        {
            $result->where('user_gender_id', $request->gender_id);
        }

        if(! is_null($request->rate_id))
        {
            $result->whereHas('rate', function($query) use ($request){
                $query->where('rate_id', $request->rate_id); 
            });
        }

        if(! is_null($request->skin_color))
        {
            $result->where('skin_color_id', $request->skin_color);
        }

        if(! is_null($request->eye_color))
        {
            $result->where('eye_color_id', $request->eye_color);
        }

        if(! is_null($request->hair_color))
        {
            $result->where('hair_color_id', $request->hair_color);
        }

        if(! is_null($request->height_from))
        {
            $result->where('height', '>=', (int)$request->height_from);
        }

        if(! is_null($request->height_to))
        {
            $result->where('height', '<=', (int)$request->height_to);
        }

        if(! is_null($request->weight_from))
        {
            $result->where('weight', '>=', (int)$request->weight_from);
        }

        if(! is_null($request->weight_to))
        {
            $result->where('weight', '<=', (int)$request->weight_to);
        }

        if(! is_null($request->price_from))
        {
            $result->whereHas('price', function($query) use ($request){
                $query->where('price', '>=' ,(int)$request->price_from); 
            });
        }

        if(! is_null($request->price_to))
        {
            $result->whereHas('price', function($query) use ($request){
                $query->where('price', '<=' ,(int)$request->price_to); 
            });
        }

        if(! is_null($request->is_travel))
        {
            $result->where('is_travel', (int)$request->is_travel);
        }

        $users = $result->paginate(12);
        $model = $request->all();
        
        return view('app.category.result', compact('users', 'model'));
    }
}
