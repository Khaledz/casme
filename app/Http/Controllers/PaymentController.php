<?php

namespace App\Http\Controllers;

use App\Models\Payment as PaymentModel;
use App\Models\Order;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\ResultPrinter;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();

        return view('app.portofolio.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        
        session()->put('order', $order);

        $clientId = 'AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS';
        $clientSecret = 'EGnHDxD_qRPdaLdZz8iCr8N7_MzF-YHPTkjs6NKYQvQSBngp4PTTVWkPZRbL';

        $apiContext = $this->getApiContext($clientId, $clientSecret);

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $item1 = new Item();
        $item1->setName($order->beneficiary->fullName)
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($order->price / 3.75);
            

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $amount = new Amount();
        $amount->setCurrency("USD")
        ->setTotal($order->price / 3.75);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription("Order a service for " . $order->beneficiary->fullName)
        ->setInvoiceNumber(uniqid());

        $baseUrl = config('app.url');
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("$baseUrl/successful-payment?success=true")
            ->setCancelUrl("$baseUrl/successful-payment?success=false");

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $request = clone $payment;

        try {
    $response = $payment->create($apiContext);
} catch (\PayPal\Exception\PPConnectionException $pce) {
    // Don't spit out errors or use "exit" like this in production code
    echo '<pre>';print_r(json_decode($pce->getData()));exit;
}

        

        session()->put('apiContext', $apiContext);

        $approvalUrl = $payment->getApprovalLink();

        return redirect($approvalUrl);    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function show(Balance $balance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function edit(Balance $balance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Balance $balance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Balance $balance)
    {
        //
    }

    /**
     * Helper method for getting an APIContext for all calls
     * @param string $clientId Client ID
     * @param string $clientSecret Client Secret
     * @return PayPal\Rest\ApiContext
     */
    function getApiContext($clientId, $clientSecret)
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        $apiContext->setConfig(
            array(
                'mode' => 'sandbox',
                'log.LogEnabled' => true,
                'log.FileName' => '../PayPal.log',
                'log.LogLevel' => 'DEBUG', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'cache.enabled' => true,
            )
        );

        return $apiContext;
    }

    public function successful(Request $request)
    {
        $order = session('order');
        $apiContext = session('apiContext');

        $payment = new PaymentModel();
        $payment->payment_code = $request->paymentId;
        $payment->user_id = auth()->user()->id;
        $payment->payment_status_id = 1;
        $payment->payment_method_id = 2;
        $payment->order_id = $order->id;
        $payment->amount = $order->price;
        $payment->save();

        if(! is_null($request->success))
        {
            if($request->success == 'true')
            {
                $paymentId = $request->paymentId;
                $payment = Payment::get($paymentId, $apiContext);
                // $execution = new PaymentExecution();
                // $execution->setPayerId($request->paymentId);
                // $result = $payment->execute($execution, $apiContext);
                $payment = Payment::get($paymentId, $apiContext);
                return redirect(route('app.order.show', $order->id))->with('message', 'تم ارسال عملية الدفع بنجاح!');
            }
            else if($request->success == 'false')
            {
                echo 'false';
            }
        }
    }
}

