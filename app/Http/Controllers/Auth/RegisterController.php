<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Models\SocialMedia;
use App\Models\UserPortfolio;
use App\Models\Category;
use App\Models\Balance;
use MediaEmbed\MediaEmbed;
use Socialite;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/settings';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index($role)
    {
        $role = \DB::table('roles')->whereName($role)->first();

        if(is_null($role))
        {
            return redirect('/');
        }

        if($role->name == 'member')
        {
            return view('auth.member');
        }
        else if($role->name == 'beneficiary')
        {
            return view('auth.beneficiary');            
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create_beneficiary(array $data)
    {
        $user = User::create([
            'user_category_id' => $data['user_category_id'],
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'about' => $data['about'],
            'city' => $data['city'],
            'country_id' => $data['country_id'],
            'user_gender_id' => $data['user_gender_id'],
            'is_travel' => $data['is_travel'],
            'password' => Hash::make($data['password']),
            'video' => $data['video'],
            'height' => $data['height'],
            'age' => $data['age'],
            'weight' => $data['weight'],
            'eye_color_id' => $data['eye_color_id'],
            'hair_color_id' => $data['hair_color_id'],
            'skin_color_id' => $data['skin_color_id'],
            'residence_country_id' => $data['residence_country_id']
        ]);

        // if there is introduction video, we need to conver it to embed url.
        if(isset($data['video']) && ! is_null($data['video']))
        {
            $MediaEmbed = new MediaEmbed();
            $data['embed_video'] = $MediaEmbed->parseUrl($data['video'])->getEmbedCode();
            $user->update(['video' => $data['video'], 'embed_video' => $data['embed_video']]);
        }
        
        $this->storePrice($data, $user);
        $this->storeSocialMedia($data, $user);
        $this->storeAccents($data, $user);
        $this->storePortfolio($data, $user);
        $this->handleUploads($data, $user);

        return $user;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create_member(array $data)
    {
        $user = User::create([
            'fname' => $data['fname'],
            'lname' => $data['lname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'about' => $data['about'],
            'city' => $data['city'],
            'country_id' => $data['country_id'],
            'user_gender_id' => $data['user_gender_id'],
            'password' => Hash::make($data['password']),
            'residence_country_id' => $data['residence_country_id']
        ]);
        
        $this->handleUploads($data, $user);

        return $user;
    }

    public function register_member(Request $request)
    {
        $this->validate($request, [
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'country_id' => 'required|integer|exists:countries,id',
            'city' => 'required|min:3',
            'user_gender_id' => 'required|integer|exists:user_genders,id',
            'about' => 'required|max:4000|min:10',
            'agreement' => 'accepted',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'residence_country_id' => 'required|integer|exists:countries,id'
        ]);
        
        event(new Registered($user = $this->create_member($request->all())));

        $user->assignRole('member');

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath())
                        ->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! أستمتع بخدماتنا المقدمة لك');
    }

    public function register_beneficiary(Request $request)
    {
        $validations = [
            'user_category_id' => 'required|integer|exists:categories,id',
            'fname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'string', 'min:6', 'max:20'],
            'country_id' => 'required|integer|exists:countries,id',
            'user_gender_id' => 'required|integer|exists:user_genders,id',
            'is_travel' => 'nullable|integer',
            'city' => 'required|max:255',
            'about' => 'required|max:4000|min:10',
            'facebook' => 'nullable|max:4000|min:3',
            'twitter' => 'nullable|max:4000|min:3',
            'instagram' => 'nullable|max:4000|min:3',
            'snapchat' => 'nullable|max:4000|min:3',
            'agreement' => 'accepted',
            'residence_country_id' => 'required|integer|exists:countries,id'
        ];

        if(! is_null($request->user_category_id))
        {
            $requirements = Category::find($request->user_category_id)->requirements;

            if($requirements->count() > 0)
            {
                foreach($requirements as $requirement)
                {
                    $validations = array_merge($validations, $requirement->validations);
                }
            }
        }

        $this->validate($request, $validations);

        event(new Registered($user = $this->create_beneficiary($request->all())));
        
        $user->assignRole('beneficiary');

        $balance = new Balance();
        $balance->user_id = $user->id;
        $balance->save();

        $this->guard()->login($user);

        session()->flash('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا!.');

        return $this->registered($request, $user);
    }

    private function storeSocialMedia($data, $user)
    {
        if(! is_null($data['facebook']))
        {
            $facebook = SocialMedia::where('english_name', 'facebook')->first();
            $user->socials()->attach($facebook, ['url' => $data['facebook']]);
        }

        if(! is_null($data['instagram']))
        {
            $instagram = SocialMedia::where('english_name', 'instagram')->first();
            $user->socials()->attach($instagram, ['url' => $data['instagram']]);
        }

        if(! is_null($data['twitter']))
        {
            $twitter = SocialMedia::where('english_name', 'twitter')->first();
            $user->socials()->attach($twitter, ['url' => $data['twitter']]);
        }

        if(! is_null($data['snapchat']))
        {
            $snapchat = SocialMedia::where('english_name', 'snapchat')->first();
            $user->socials()->attach($snapchat, ['url' => $data['snapchat']]);            
        }
    }

    private function storePrice($data, $user)
    {
        if(!is_null($data['price_type_id']))
        {
            \App\Models\UserPrice::create([
                'price_type_id' => $data['price_type_id'],
                'price' => $data['price'],
                'user_id' => $user->id
            ]);
        }
    }

    private function storeAccents($data, $user)
    {
        if(! is_null($data['accents']))
        {
            $user->accents()->attach(explode(',', $data['accents']));
        }
    }

    private function storePortfolio($data, $user)
    {
        if(! is_null($data['portfolio_title']) && ! is_null($data['portfolio_url']))
        {
            $portfolio = new UserPortfolio();
            $portfolio->title = $data['portfolio_title'];
            $portfolio->url = $data['portfolio_url'];
            $portfolio->user_id = $user->id;
            $portfolio->save();
            $user->portfolios()->save($portfolio);
        }
    }

    public function handleUploads($data, $user)
    {
        if(request()->hasFile('back'))
        {
            request()->file('back')->store('public');
            $file = request()->file('back')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('back')->getClientOriginalName(),
                    'description' => null,
                    'type_id' => 3,
                    'model' => 'App\Models\User',
                    'user_id' => $user->id,
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }

        if(request()->hasFile('front'))
        {
            request()->file('front')->store('public');
            $file = request()->file('front')->hashName();
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('front')->getClientOriginalName(),
                    'description' => null,
                    'user_id' => $user->id,
                    'type_id' => 2,
                    'model' => 'App\Models\User',
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }

        if(request()->hasFile('avatar'))
        {
            request()->file('avatar')->store('public');
            $file = request()->file('avatar')->hashName();
            $user->update(['avatar' => $file]);
            $attachment = \App\Models\Attachment::create(
                [
                    'name' => $file,
                    'orginal_name' => request()->file('avatar')->getClientOriginalName(),
                    'description' => null,
                    'user_id' => $user->id,
                    'type_id' => 6,
                    'model' => 'App\Models\User',
                    'model_id' => $user->id
                ]
            );

            $user->attachments()->save($attachment);
        }
    }

    private function convertYoutubeToEmbed($url)
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id;
    }

    private function get_domain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::with($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $userSocial = Socialite::driver($provider)->stateless()->user();

        if($provider == 'facebook')
        {
            if(session('role') == 'member')
            {
                $authUser = User::where('email', $userSocial->email)->first();
                if ($authUser) {
                    return redirect('/')->with('message', 'لديك حساب مسجل بهذه المعلومات مسبقاً');
                }

                $user = User::create([
                    'fname'     => explode(' ', $userSocial->name)[0],
                    'lname'     => explode(' ', $userSocial->name)[1],
                    'email'    => $userSocial->email,
                    'provider' => $provider,
                    'provider_id' => $userSocial->id
                ]);

                $user->assignRole('member');

                \Auth::login($user);

                // $user->sendEmailVerificationNotification(true);

                return redirect(route('app.user.setting'))->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! لخدمتك بشكل أفضل قم بتعبئة بقية الحقول.');
            }
            else
            {
                if(session('role') == 'beneficiary')
                {
                    $authUser = User::where('email', $userSocial->email)->first();
                    if ($authUser) {
                        return redirect('/')->with('message', 'لديك حساب مسجل بهذه المعلومات مسبقاً');
                    }
                    $user = User::create([
                        'fname'     => explode(' ', $userSocial->name)[0],
                        'lname'     => explode(' ', $userSocial->name)[1],
                        'email'    => $userSocial->email,
                        'provider' => $provider,
                        'provider_id' => $userSocial->id
                    ]);

                    $user->assignRole('beneficiary');

                    \Auth::login($user);

                    $balance = new Balance();
        $balance->user_id = $user->id;
        $balance->save();

                    // $user->sendEmailVerificationNotification(true);

                    return redirect(url('/set-category'))->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! من فضلك حدد فئتك.');
                }
            }
        }
        elseif($provider == 'twitter')
        {
            if(session('role') == 'member')
            {
                $authUser = User::where('provider_id', $userSocial->id)->first();
                
                if ($authUser) {
                    return redirect('/')->with('message', 'لديك حساب مسجل بهذه المعلومات مسبقاً');
                }
                
                $user = User::create([
                    'fname'     => explode(' ', $userSocial->name)[0],
                    'lname'     => explode(' ', $userSocial->name)[1],
                    // 'email'    => $userSocial->email,
                    'provider' => $provider,
                    'provider_id' => $userSocial->id
                ]);

                $user->assignRole('member');

                \Auth::login($user);

                // $user->sendEmailVerificationNotification(true);

                return redirect(route('app.user.setting'))->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! لخدمتك بشكل أفضل قم بتعبئة بقية الحقول.');
            }
            else if(session('role') == 'beneficiary')
            {
                $authUser = User::where('email', $userSocial->email)->first();
                if ($authUser) {
                    return redirect('/')->with('message', 'لديك حساب مسجل بهذه المعلومات مسبقاً');
                }
                $user = User::create([
                    'fname'     => explode(' ', $userSocial->name)[0],
                    'lname'     => explode(' ', $userSocial->name)[1],
                    // 'email'    => $userSocial->email,
                    'provider' => $provider,
                    'provider_id' => $userSocial->id
                ]);

                $user->assignRole('beneficiary');

                \Auth::login($user);

                $balance = new Balance();
        $balance->user_id = $user->id;
        $balance->save();

                // $user->sendEmailVerificationNotification(true);

                return redirect(url('/set-category'))->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! من فضلك حدد فئتك.');
            }
        }
        else if($provider == 'instagram')
        {
            if(session('role') == 'member')
            {
                $authUser = User::where('provider_id', $userSocial->id)->first();
                if ($authUser) {
                    return redirect('/')->with('message', 'لديك حساب مسجل بهذه المعلومات مسبقاً');
                }
    
                $user = User::create([
                    'fname'     => explode(' ', $userSocial->name)[0],
                    'lname'     => explode(' ', $userSocial->name)[1],
                    // 'email'    => $userSocial->email,
                    'provider' => $provider,
                    'provider_id' => $userSocial->id
                ]);

                $user->assignRole('member');

                \Auth::login($user);

                // $user->sendEmailVerificationNotification(true);

                return redirect(route('app.user.setting'))->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! لخدمتك بشكل أفضل قم بتعبئة بقية الحقول.');
            }
            else if(session('role') == 'beneficiary')
            {
                $authUser = User::where('email', $userSocial->email)->first();
                if ($authUser) {
                    return redirect('/')->with('message', 'لديك حساب مسجل بهذه المعلومات مسبقاً');
                }
                $user = User::create([
                    'fname'     => explode(' ', $userSocial->name)[0],
                    'lname'     => explode(' ', $userSocial->name)[1],
                    // 'email'    => $userSocial->email,
                    'provider' => $provider,
                    'provider_id' => $userSocial->id
                ]);

                $user->assignRole('beneficiary');

                \Auth::login($user);

                $balance = new Balance();
                $balance->user_id = $user->id;
                $balance->save();

                // $user->sendEmailVerificationNotification(true);

                return redirect(url('/set-category'))->with('message', 'شكراً ' . $user->fullName .' لتسجيلك معنا! من فضلك حدد فئتك.');
            }
        }

        
        session()->forget('role');
    }

    
}
