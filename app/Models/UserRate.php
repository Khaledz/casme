<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRate extends Model
{
    protected $guarded = [];

    public $timestamps = false;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function rate()
    {
        return $this->belongsTo('App\Models\Rate');
    }
}
