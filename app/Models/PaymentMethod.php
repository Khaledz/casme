<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class PaymentMethod extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }
}
