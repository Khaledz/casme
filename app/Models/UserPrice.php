<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class UserPrice extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public function type()
    {
        return $this->belongsTo('App\Models\PriceType', 'price_type_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
