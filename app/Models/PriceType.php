<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class PriceType extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public function prices()
    {
        return $this->hasMany('App\Models\UserPrice');
    }
}
