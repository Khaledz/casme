<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SocialMedia extends Model
{
    use HasTranslations;

    protected $guarded = [];

    protected $table = 'social_media';

    // public $translatable = ['name'];

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'users_socials', 'user_id', 'social_media_id')->withPivot([
            'url'
        ]);
    }
}
