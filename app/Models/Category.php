<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasTranslations;

    protected $with = ['users'];

    protected $guarded = [];

    public $translatable = ['name'];

    protected $appends = ['background'];

    public function users()
    {
        return $this->hasMany('App\Models\User', 'user_category_id')->role('beneficiary')
        ->whereNotNull('fname')
        ->whereNotNull('lname')
        ->whereNotNull('country_id')
        ->whereNotNull('residence_country_id')
        ->whereNotNull('city')
        ->whereNotNull('user_gender_id')
        ->whereNotNull('email')
        ->whereNotNull('user_category_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\CategoryType', 'category_type_id');
    }

    public function requirements()
    {
        return $this->belongsToMany('App\Models\CategoryRequirement', 'categories_requirements', 'category_id', 'category_requirement_id');
    }

    public function attachment()
    {
        return $this->morphOne('App\Models\Attachment', 'model', 'model');
    }

    public static function getHomeCategories()
    {
        return self::where('in_home', true)->get();
    }

    public function getBackgroundAttribute()
    {
        $file = Attachment::where('type_id', 7)->where('model_id', $this->id)->where('model', 'App\Models\Category')->first();
        if(! is_null($file))
        {
            return url('storage/'. $file->name);
        }
        return null;
    }
}
