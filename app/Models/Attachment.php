<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attachments';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'orginal_name', 'model', 'model_id', 'user_id', 'extension', 'type_id'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($attachment) {
            if(auth()->user())
            {
                $attachment->user_id = auth()->user()->id;
            }
        });
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\AttachmentType');
    }
}
