<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class CategoryType extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }
}
