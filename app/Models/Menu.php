<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Menu extends Model
{
    use HasTranslations;

    protected $guarded = [];

    protected $with = ['children'];

    public $translatable = ['name'];

    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Menu', 'parent_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
