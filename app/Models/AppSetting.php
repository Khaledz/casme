<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AppSetting extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['website_title', 'copyright', 'about'];

    protected $table = 'app_settings';
}
