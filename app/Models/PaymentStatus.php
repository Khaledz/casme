<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class PaymentStatus extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }
}
