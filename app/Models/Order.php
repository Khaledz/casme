<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    protected $with = ['user', 'beneficiary'];

    protected $appends = ['status'];

    public function status()
    {
        return $this->belongsTo('App\Models\OrderStatus', 'order_status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function beneficiary()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getStatusAttribute()
    {
        return $this->status()->first()->name;
    }

    // public function getProfitAttribute()
    // {
    //     return ($this->profit_margin / 100) * $this->price;
    // }
}
