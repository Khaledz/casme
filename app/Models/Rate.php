<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Rate extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public function rates()
    {
        return $this->hasMany('App\Models\UserRate');
    }
}
