<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class UserHairColor extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
