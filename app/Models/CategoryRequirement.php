<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class CategoryRequirement extends Model
{
    use HasTranslations;

    protected $guarded = [];

    public $translatable = ['name'];

    protected $casts = ['validations' => 'array'];

    public function categories()
    {
        return $this->belongsToMany('App\Models\CategoryRequirement', 'categories_requirements', 'category_requirement_id', 'category_id');
    }
}
