<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use App\Notifications\VerifyUser;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasRoles, HasApiTokens;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with = ['country', 'skin', 'hair', 'gender', 'eye', 'resident', 'portfolios', 'accents', 'price', 'rate', 'socials', 'balance'];

    protected $appends = ['avatar_url', 'front_hand_url', 'back_hand_url', 'role', 'category_name', 'price_type'];

    public function avatar()
    {
        if(is_null($this->avatar) && !file_exists(asset('storage/'. $this->avatar)))
        {
            return asset('storage/no-avatar.png');
        }
        else
        {
            return asset('storage/'. $this->avatar);
        }
    }

    public function getCategoryNameAttribute()
    {
        if(! is_null($this->category()->first()))
        {
            return $this->category()->first()->name;
        }
        else
        {
            return null;
        }
    }

    public function getIsTravelAttribute($value)
    {
        if(is_null($value))
        {
            return __('general.notcare');
        }
        else if($value == 0)
        {
            return __('general.no');
        }
        else if($value == 1)
        {
            return __('general.yes');
        }
    }

    public function getRoleNames(): Collection
    {
        return $this->roles->pluck('display_name');
    }

    public function status()
    {
        return $this->status == 1 ? 'مفعل' : 'غير مفعل';
    }

    public function getFullNameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'user_category_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function resident()
    {
        return $this->belongsTo('App\Models\Country', 'residence_country_id');
    }

    public function accents()
    {
        return $this->belongsToMany('App\Models\UserAccent', 'users_accents', 'user_id', 'accent_id');
    }

    public function price()
    {
        return $this->hasOne('App\Models\UserPrice');
    }

    public function gender()
    {
        return $this->belongsTo('App\Models\UserGender', 'user_gender_id');
    }

    public function attachments()
    {
        return $this->hasMany('App\Models\Attachment');
    }

    public function portfolios()
    {
        return $this->hasMany('App\Models\UserPortfolio');
    }

    public function rate()
    {
        return $this->hasOne('App\Models\UserRate');
    }

    public function eye()
    {
        return $this->belongsTo('App\Models\UserEyeColor', 'eye_color_id');
    }

    public function hair()
    {
        return $this->belongsTo('App\Models\UserHairColor', 'hair_color_id');
    }

    public function skin()
    {
        return $this->belongsTo('App\Models\UserSkinColor', 'skin_color_id');
    }

    public function socials()
    {
        return $this->belongsToMany('App\Models\SocialMedia', 'users_socials', 'user_id', 'social_media_id')->withPivot([
            'url'
        ]);
    }

    public function video()
    {
        return Attachment::where('type_id', 4)->where('user_id', $this->id)->first();
    }

    public function getFrontHand()
    {
        $file = Attachment::where('type_id', 2)->where('user_id', $this->id)->latest()->first();

        if(! is_null($file))
        {
            return asset('storage/' . $file->name);
        }
        return asset('storage/no-avatar.png');
    }

    public function getBackHand()
    {
        $file = Attachment::where('type_id', 3)->where('user_id', $this->id)->latest()->first();

        if(! is_null($file))
        {
            return asset('storage/' . $file->name);
        }
        return asset('storage/no-avatar.png');
    }

    public function balance()
    {
        return $this->hasOne('App\Models\Balance', 'user_id');
    }

    public function getAvatarUrlAttribute()
    {
        return url('storage/'. $this->attributes['avatar']);
    }

    public function getFrontHandUrlAttribute()
    {
        $file = Attachment::where('type_id', 2)->where('user_id', $this->id)->latest()->first();
        if(! is_null($file))
        {
            return url('storage/'. $file->name);
        }
        return null;
    }

    public function getBackHandUrlAttribute()
    {
        $file = Attachment::where('type_id', 3)->where('user_id', $this->id)->latest()->first();
        if(! is_null($file))
        {
            return url('storage/'. $file->name);
        }
        return null;
    }

    public function getRoleAttribute()
    {
        return $this->getRoleNames();
    }

    public function getPriceTypeAttribute()
    {
        if(! is_null($this->price))
        return $this->price->type->name;
    }
}
